-- phpMyAdmin SQL Dump
-- version 4.9.7deb1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mar. 31 mai 2022 à 14:40
-- Version du serveur :  8.0.27-0ubuntu0.21.04.1
-- Version de PHP : 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gsb`
--

-- --------------------------------------------------------

--
-- Structure de la table `etats`
--

CREATE TABLE `etats` (
  `id` int NOT NULL,
  `libelle` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `etats`
--

INSERT INTO `etats` (`id`, `libelle`) VALUES
(1, 'archivée'),
(2, 'validée'),
(3, 'envoyée'),
(4, 'brouillon');

-- --------------------------------------------------------

--
-- Structure de la table `fiches_frais`
--

CREATE TABLE `fiches_frais` (
  `id` int NOT NULL,
  `mois` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `annee` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `etat` int NOT NULL,
  `utilisateurId` int NOT NULL,
  `dateModification` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `fiches_frais`
--

INSERT INTO `fiches_frais` (`id`, `mois`, `annee`, `etat`, `utilisateurId`, `dateModification`) VALUES
(56, '04', '2022', 3, 1, '2022-05-15 08:56:49'),
(57, '05', '2022', 4, 1, '2022-05-18 05:35:34');

-- --------------------------------------------------------

--
-- Structure de la table `frais_en_forfait`
--

CREATE TABLE `frais_en_forfait` (
  `id` int NOT NULL,
  `libelle` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `prixUnitaire` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `frais_en_forfait`
--

INSERT INTO `frais_en_forfait` (`id`, `libelle`, `prixUnitaire`) VALUES
(1, 'Trajet Train', '14.25'),
(2, 'Nuit hotel', '25.00'),
(3, 'Kilometre voiture', '0.20');

-- --------------------------------------------------------

--
-- Structure de la table `justificatifs`
--

CREATE TABLE `justificatifs` (
  `id` int NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `dateUpload` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `lignes_frais_en_forfait`
--

CREATE TABLE `lignes_frais_en_forfait` (
  `id` int NOT NULL,
  `quantite` int DEFAULT NULL,
  `fraisEnForfaitId` int DEFAULT NULL,
  `ficheFraisId` int DEFAULT NULL,
  `justificatifId` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `lignes_frais_en_forfait`
--

INSERT INTO `lignes_frais_en_forfait` (`id`, `quantite`, `fraisEnForfaitId`, `ficheFraisId`, `justificatifId`) VALUES
(36, 1, 1, 56, NULL),
(37, 2, 1, 57, NULL),
(38, 1, 2, 57, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `lignes_frais_hors_forfait`
--

CREATE TABLE `lignes_frais_hors_forfait` (
  `id` int NOT NULL,
  `libelle` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `quantite` int DEFAULT NULL,
  `prixUnitaire` float DEFAULT NULL,
  `ficheFraisId` int DEFAULT NULL,
  `justificatifId` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `lignes_frais_hors_forfait`
--

INSERT INTO `lignes_frais_hors_forfait` (`id`, `libelle`, `quantite`, `prixUnitaire`, `ficheFraisId`, `justificatifId`) VALUES
(12, 'Photocopies', 20, 0.2, 56, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int NOT NULL,
  `libelle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `libelle`) VALUES
(1, 'visiteur'),
(2, 'comptable'),
(3, 'administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `id` int NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `identifiant` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `motDePasse` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `dateNaissance` date DEFAULT NULL,
  `dateEmbauche` date NOT NULL,
  `roleId` int NOT NULL,
  `adresse` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `codePostal` char(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ville` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `nom`, `prenom`, `email`, `identifiant`, `motDePasse`, `dateNaissance`, `dateEmbauche`, `roleId`, `adresse`, `codePostal`, `ville`) VALUES
(1, 'Dupuis', 'Jean', 'dupuis@gsb.com', 'visit ', '$2b$10$KkjZU2Xy6d84wKhcEWCwh..SjvGeU.jJL1B0O8dJnj3bBI1jnQRui', '1988-11-15', '2021-11-17', 1, '', '', ''),
(2, 'Latour', 'Sophie', 'latour@gsb.com', 'compta', '$2b$10$tQ9PTauk/q/1czetBeUxVeRpT/sV/51DkFCwVWB6ZR3Oa74u64YUC', '1989-08-10', '2005-12-21', 2, NULL, NULL, NULL),
(3, 'Delbrel', 'Julien', 'delbrel@gmail.com', 'admin', '$2b$10$560ryHpyJNfkF83JuHwTjOgv1lA66U5p5oWfJHGTZBN8LK06vNwpq', '1972-01-18', '1998-11-23', 3, NULL, NULL, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `etats`
--
ALTER TABLE `etats`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `fiches_frais`
--
ALTER TABLE `fiches_frais`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_fiche_frais_utilisateur` (`utilisateurId`),
  ADD KEY `fk_fiche_frais_etatfichefrais` (`etat`);

--
-- Index pour la table `frais_en_forfait`
--
ALTER TABLE `frais_en_forfait`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `justificatifs`
--
ALTER TABLE `justificatifs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `lignes_frais_en_forfait`
--
ALTER TABLE `lignes_frais_en_forfait`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fraisForfaitisesID` (`fraisEnForfaitId`),
  ADD KEY `ficheFraisID` (`ficheFraisId`),
  ADD KEY `lignes_frais_en_forfait_ibfk_3` (`justificatifId`);

--
-- Index pour la table `lignes_frais_hors_forfait`
--
ALTER TABLE `lignes_frais_hors_forfait`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ficheFraisID` (`ficheFraisId`),
  ADD KEY `lignes_frais_hors_forfait_ibfk_2` (`justificatifId`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_utilisateur_role` (`roleId`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `etats`
--
ALTER TABLE `etats`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `fiches_frais`
--
ALTER TABLE `fiches_frais`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT pour la table `frais_en_forfait`
--
ALTER TABLE `frais_en_forfait`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `justificatifs`
--
ALTER TABLE `justificatifs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `lignes_frais_en_forfait`
--
ALTER TABLE `lignes_frais_en_forfait`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT pour la table `lignes_frais_hors_forfait`
--
ALTER TABLE `lignes_frais_hors_forfait`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `fiches_frais`
--
ALTER TABLE `fiches_frais`
  ADD CONSTRAINT `fk_fiche_frais_etatfichefrais` FOREIGN KEY (`etat`) REFERENCES `etats` (`id`),
  ADD CONSTRAINT `fk_fiche_frais_utilisateur` FOREIGN KEY (`utilisateurId`) REFERENCES `utilisateurs` (`id`);

--
-- Contraintes pour la table `lignes_frais_en_forfait`
--
ALTER TABLE `lignes_frais_en_forfait`
  ADD CONSTRAINT `lignes_frais_en_forfait_ibfk_1` FOREIGN KEY (`ficheFraisId`) REFERENCES `fiches_frais` (`id`),
  ADD CONSTRAINT `lignes_frais_en_forfait_ibfk_2` FOREIGN KEY (`fraisEnForfaitId`) REFERENCES `frais_en_forfait` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `lignes_frais_en_forfait_ibfk_3` FOREIGN KEY (`justificatifId`) REFERENCES `justificatifs` (`id`);

--
-- Contraintes pour la table `lignes_frais_hors_forfait`
--
ALTER TABLE `lignes_frais_hors_forfait`
  ADD CONSTRAINT `lignes_frais_hors_forfait_ibfk_1` FOREIGN KEY (`ficheFraisId`) REFERENCES `fiches_frais` (`id`),
  ADD CONSTRAINT `lignes_frais_hors_forfait_ibfk_2` FOREIGN KEY (`justificatifId`) REFERENCES `justificatifs` (`id`);

--
-- Contraintes pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD CONSTRAINT `fk_utilisateur_role` FOREIGN KEY (`roleId`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
