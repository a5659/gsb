import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cards-services',
  templateUrl: './cards-services.component.html',
  styleUrls: ['./cards-services.component.scss']
})
export class CardsServicesComponent implements OnInit {
  title = 'AwesomeGSB'; /* Ici, on a le nom du projet, c'est ce qui va apparaitre dans l'onglet du navigateur */
  LogoGSB = "assets/LogoGSB3.jpg";
  Lab = "assets/Lab.jpg";
  Fleche = "assets/Fleche.png"
  constructor() { }

  ngOnInit(): void {
  }

}
