import { Component, OnInit } from '@angular/core';
import {Observable, OperatorFunction} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';

const villes = ['Bordeaux', 'Brest', '', 'Lyon', 'Marseille', 'Nantes', 'Paris',
  'Rennes', 'Toulouse'];

@Component
({
  selector: 'app-rendez-vous',
  templateUrl: './rendez-vous.component.html',
  styleUrls: ['./rendez-vous.component.scss'],
  styles: [`.form-control { width: 300px; }`]
})

export class RendezVousComponent implements OnInit 
{
  title = 'AwesomeGSB'; /* Ici, on a le nom du projet, c'est ce qui va apparaitre dans l'onglet du navigateur */
  LogoGSB = "assets/LogoGSB3.jpg";
  Lab = "assets/Lab.jpg";
  public model: any;
  constructor() { }

  ngOnInit(): void {
  }

  search: OperatorFunction<string, readonly string[]> = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term.length < 2 ? []
      : villes.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  )
  
}