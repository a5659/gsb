import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OngletsConnectionComponent } from './onglets-connection/onglets-connection.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PresentationGSBComponent } from './presentation-gsb/presentation-gsb.component';
import { CardsServicesComponent } from './cards-services/cards-services.component';
import { ContactsComponent } from './contacts/contacts.component';
import { NavbarPagesComponent } from './navbar-pages/navbar-pages.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { AuthService } from './services/auth.service';
import { FichesService } from './services/fiche.service';
import { UtilisateursService } from './services/utilisateur.service';
import { FraisEnForfaitService } from './services/fraisEnForfait.service';
import { LigneFraisEnForfaitsService } from './services/ligneFraisEnForfait.service';
import { LigneFraisHorsForfaitsService } from './services/ligneFraisHorsForfait.service';

import { FicheIndexComponent } from './fiche-index/fiche-index.component';
import { FicheCreateComponent } from './fiche-create/fiche-create.component';
import { FicheEditComponent } from './fiche-edit/fiche-edit.component';
import { AdminIndexComponent } from './admin-index/admin-index.component';
import { UtilisateurEditComponent } from './utilisateur-edit/utilisateur-edit.component';
import { UtilisateurCreateComponent } from './utilisateur-create/utilisateur-create.component';

import { RendezVousComponent } from './rendez-vous/rendez-vous.component';
import { AccueilComponent } from './accueil/accueil.component';
import { MapComponent } from './map/map.component';
import { LabosComponent } from './labos/labos.component';
import { ProAccessComponent } from './pro-access/pro-access.component';
import { PatientAccessComponent } from './patient-access/patient-access.component';
import { FormulaireContactComponent } from './formulaire-contact/formulaire-contact.component';
import { LigneEnForfaitComponent } from './ligne-en-forfait/ligne-en-forfait.component';
import { LigneHorsForfaitComponent } from './ligne-hors-forfait/ligne-hors-forfait.component';
import { FicheIndexComptableComponent } from './fiche-index-comptable/fiche-index-comptable.component';
import { FicheEditComptableComponent } from './fiche-edit-comptable/fiche-edit-comptable.component';
import { FraisIndexComponent } from './frais-index/frais-index.component';
import { FraisEnForfaitFormComponent } from './frais-en-forfait-form/frais-en-forfait-form.component';
import { LogoutButtonComponent } from './logout-button/logout-button.component';

@NgModule({
  declarations: [
    AppComponent,
    OngletsConnectionComponent,
    NavbarComponent,
    PresentationGSBComponent,
    CardsServicesComponent,
    ContactsComponent,
    NavbarPagesComponent,
    FicheIndexComponent,
    FicheCreateComponent,
    FicheEditComponent,
    RendezVousComponent,
    AccueilComponent,
    MapComponent,
    LabosComponent,
    ProAccessComponent,
    PatientAccessComponent,
    FormulaireContactComponent,
    LigneEnForfaitComponent,
    LigneHorsForfaitComponent,
    AdminIndexComponent,
    UtilisateurEditComponent,
    UtilisateurCreateComponent,
    FicheIndexComptableComponent,
    FicheEditComptableComponent,
    FraisIndexComponent,
    FraisEnForfaitFormComponent,
    LogoutButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    AuthService,
    FichesService,
    CookieService,
    FormsModule,
    FraisEnForfaitService,
    LigneFraisEnForfaitsService,
    LigneFraisHorsForfaitsService,
    UtilisateursService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
