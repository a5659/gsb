import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PresentationGSBComponent } from './presentation-gsb.component';

describe('PresentationGSBComponent', () => {
  let component: PresentationGSBComponent;
  let fixture: ComponentFixture<PresentationGSBComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PresentationGSBComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentationGSBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
