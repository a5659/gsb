import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientAccessComponent } from './patient-access.component';

describe('PatientAccessComponent', () => {
  let component: PatientAccessComponent;
  let fixture: ComponentFixture<PatientAccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientAccessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
