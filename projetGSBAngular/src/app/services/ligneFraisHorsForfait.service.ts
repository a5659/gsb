import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class LigneFraisHorsForfaitsService {

  constructor(
    private httpClient: HttpClient,
    private cookieService: CookieService,
  ) {}

  public createLigne(payload: Record<string,any>): Observable<Record<string,any>>{
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .post<Record<string,any>>(`${environment.apiURL}/lignesFraisHorsForfait`, payload, {headers})
  }

  public updateLigne(id: number, payload: Record<string,any>): Observable<Record<string,any>>{
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .put<Record<string,any>>(`${environment.apiURL}/lignesFraisHorsForfait/${id}`, payload, {headers})
  }

  public deleteLigne(id: number): Observable<any>{
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .delete<Record<string,any>>(`${environment.apiURL}/lignesFraisHorsForfait/${id}`, {headers})
  }

}
