import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthService {

  constructor(
    private httpClient: HttpClient,
    private cookieService: CookieService,
  ) {}

  login(identifiant: string, motDePasse: string) : Observable<Record<string,any>>{
    return this.httpClient
      .post<Record<string,any>>(`${environment.apiURL}/login`, {identifiant, motDePasse})

  }
  authAdmin(): Observable<any>{
      let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
      return this.httpClient
        .post<any>(`${environment.apiURL}/authAdmin`, {headers});
  }

  authComptable(): Observable<any>{
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .post<any>(`${environment.apiURL}/authComptable`, {headers});
  }

  authVisiteur(): Observable<any>{
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .post<any>(`${environment.apiURL}/authVisiteur`, {headers});
  }
}
