import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

import { Fiche } from '../classes/fiche.class';

@Injectable()
export class FichesService {

  constructor(
    private httpClient: HttpClient,
    private cookieService: CookieService,
    ) {}

  public createFiche(payload: Record<string,any>): Observable<Fiche>{
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient.post<Fiche>(`${environment.apiURL}/fiches`, payload, {headers});
  }

  public getFichesByVisiteurId(visiteurId: number): Observable<Fiche[]>{
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .get<Fiche[]>(`${environment.apiURL}/fiches/visiteur/${visiteurId}`, {headers});
  }

  public getFiche(id: number): Observable<Fiche>{
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .get<Fiche>(`${environment.apiURL}/fiches/${id}`, {headers});
  }

  public updateFiche(id: number, payload: Record<string,any>): Observable<Fiche>{
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .put<Fiche>(`${environment.apiURL}/fiches/${id}`, payload, {headers})
  }

}
