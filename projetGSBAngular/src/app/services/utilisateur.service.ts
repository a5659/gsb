import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

import { Utilisateur } from '../classes/utilisateur.class';

@Injectable()
export class UtilisateursService {

  constructor(
    private httpClient: HttpClient,
    private cookieService: CookieService,
  ) {}

  public createUtilisateur(payload: Record<string,any>): Observable<Utilisateur> {
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .post<Utilisateur>(`${environment.apiURL}/utilisateurs`, payload, {headers});
  }

  public getUtilisateur(id: number): Observable<Utilisateur> {
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .get<Utilisateur>(`${environment.apiURL}/utilisateurs/${id}`, {headers});
  }

  public getUtilisateurs(): Observable<Utilisateur[]> {
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .get<Utilisateur[]>(`${environment.apiURL}/utilisateurs`, {headers});
  }

  public getVisiteurs(): Observable<Utilisateur[]> {
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .get<Utilisateur[]>(`${environment.apiURL}/visiteurs`, {headers});
  }

  public updateUtilisateur(id: number, payload: Record<string,any>): Observable<Utilisateur> {
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .put<Utilisateur>(`${environment.apiURL}/utilisateurs/${id}`, payload, {headers})
  }

}
