import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

import { FraisEnForfait } from '../classes/fraisEnForfait.class';

@Injectable()
export class FraisEnForfaitService {

  constructor(
    private httpClient: HttpClient,
    private cookieService: CookieService,
  ) {}

  public createFraisEnForfait(payload: Record<string,any>): Observable<FraisEnForfait>{
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .post<FraisEnForfait>(`${environment.apiURL}/fraisEnForfait`, payload, {headers});
  }

  public getAllFraisEnForfait(): Observable<FraisEnForfait[]>{
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .get<FraisEnForfait[]>(`${environment.apiURL}/fraisEnForfait`, {headers})
  }

  public updateFraisEnForfait(id: number, payload: Record<string,any>): Observable<FraisEnForfait>{
    let token = this.cookieService.get('accessToken');
    let headers = new HttpHeaders().set('x-access-token', token);
    return this.httpClient
      .put<FraisEnForfait>(`${environment.apiURL}/fraisEnForfait/${id}`, payload, {headers})
  }


}
