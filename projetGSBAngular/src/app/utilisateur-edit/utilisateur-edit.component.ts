import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

import { Utilisateur } from '../classes/utilisateur.class';
import { UtilisateursService } from '../services/utilisateur.service';

@Component({
  selector: 'app-utilisateur-edit',
  templateUrl: './utilisateur-edit.component.html',
  styleUrls: ['./utilisateur-edit.component.scss']
})
export class UtilisateurEditComponent implements OnInit {
  utilisateurId: any;
  utilisateur: Utilisateur = new Utilisateur(-1, "", "", new Date(), 1, "", "");
  dateEmbaucheJSON: any = {"year": 2020, "month": 1, "day": 1};
  dateNaissanceJSON?: any = null;


  constructor(
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router,
    private utilisateursService: UtilisateursService,
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.utilisateurId = this.route.snapshot.paramMap.get('id');
    })

    this.utilisateursService.getUtilisateur(this.utilisateurId)
      .subscribe(response => {
        this.utilisateur= new Utilisateur(
          response.id,
          response.nom,
          response.prenom,
          response.dateEmbauche,
          response.roleId,
          response.identifiant,
          response.motDePasse,
          response.email,
          response.dateNaissance,
          response.adresse,
          response.codePostal,
          response.ville
        );
        let dateEmbauche = new Date(response.dateEmbauche);
        this.dateEmbaucheJSON = {
          year: dateEmbauche.getFullYear(),
          month: dateEmbauche.getMonth() +1,
          day: dateEmbauche.getDate(),
        };
        if (!!response.dateNaissance) {
          let dateNaissance = new Date(response.dateNaissance);
          this.dateNaissanceJSON = {
            year: dateNaissance.getFullYear(),
            month: dateNaissance.getMonth() +1,
            day: dateNaissance.getDate(),
          };
        }
      })
  }

  confirmerUtilisateur(){
    try{
      let {id, ...payload} = this.utilisateur;
      let year = "" + this.dateEmbaucheJSON.year;
      let month = (this.dateEmbaucheJSON.month).toString().padStart(2, '0');
      let day = (this.dateEmbaucheJSON.day).toString().padStart(2, '0');
      payload.dateEmbauche = `${year}-${month}-${day}`;
      if (!!this.dateNaissanceJSON) {
        let year = "" + this.dateNaissanceJSON.year;
        let month = (this.dateNaissanceJSON.month).toString().padStart(2, '0');
        let day = (this.dateNaissanceJSON.day).toString().padStart(2, '0');
        payload.dateNaissance = `${year}-${month}-${day}`;
      }

      this.utilisateursService.updateUtilisateur(id, payload).subscribe();

      this.toastr.success("Modification effectuée", "Succès!");
      this.router.navigateByUrl('admin/index')
    } catch (error) {
      this.toastr.error("Une erreur est survenue", "Oups!");
    }
  }

}
