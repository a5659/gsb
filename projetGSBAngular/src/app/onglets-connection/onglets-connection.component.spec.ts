import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OngletsConnectionComponent } from './onglets-connection.component';

describe('OngletsConnectionComponent', () => {
  let component: OngletsConnectionComponent;
  let fixture: ComponentFixture<OngletsConnectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OngletsConnectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OngletsConnectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
