import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

import { FichesService } from '../services/fiche.service';
import { LigneFraisEnForfaitsService } from '../services/ligneFraisEnForfait.service';
import { LigneFraisHorsForfaitsService } from '../services/ligneFraisHorsForfait.service';
import { Fiche } from '../classes/fiche.class';
import { LigneFraisEnForfait } from '../classes/ligneFraisEnForfait.class';
import { LigneFraisHorsForfait } from '../classes/ligneFraisHorsForfait.class';
import { LigneEnForfaitComponent } from '../ligne-en-forfait/ligne-en-forfait.component';
import { LigneHorsForfaitComponent } from '../ligne-hors-forfait/ligne-hors-forfait.component';

@Component({
  selector: 'app-fiche-edit',
  templateUrl: './fiche-edit.component.html',
  styleUrls: ['./fiche-edit.component.scss']
})
export class FicheEditComponent implements OnInit {
  ficheId: any;
  fiche: any = {};
  ligneForfaitSuprimees: Array<number> = [];
  ligneHorsForfaitSuprimees: Array<number> = [];
  title = 'AwesomeGSB'; /* Ici, on a le nom du projet, c'est ce qui va apparaitre dans l'onglet du navigateur */
  LogoGSB3 = "assets/LogoGSB3.png";
  BandeGSB2 = "assets/BandeGSB2.png"

  @ViewChildren('ligneForfait') ligneForfaitComponents?:QueryList<LigneEnForfaitComponent>;
  @ViewChildren('ligneHorsForfait') ligneHorsForfaitComponents?:QueryList<LigneHorsForfaitComponent>;

  constructor(
    private route: ActivatedRoute,
    private fichesService: FichesService,
    private ligneFraisEnForfaitsService: LigneFraisEnForfaitsService,
    private ligneFraisHorsForfaitsService: LigneFraisHorsForfaitsService,
    private toastr: ToastrService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.ficheId = this.route.snapshot.paramMap.get('id');
    });

    this.fichesService.getFiche(this.ficheId)
      .subscribe(response => {
        this.fiche = new Fiche(
          response.id,
          response.mois,
          response.annee,
          response.etat,
          response.LigneFraisEnForfaits.map(item => {
            return new LigneFraisEnForfait(
              item.id,
              item.quantite,
              item.fraisEnForfaitId,
              item.ficheFraisId
            )
          }),
          response.LigneFraisHorsForfaits.map(item => {
            return new LigneFraisHorsForfait(
              item.id,
              item.libelle,
              item.quantite,
              item.prixUnitaire,
              item.ficheFraisId,
              item.justificatifId,
            )
          }),
          response.utilisateurId,
          response.dateModification
        )
    })

  }

  verrouillee() {
    return (this.fiche.etat !== 4);
  }

  ajouterLigneEnForfait() {
    this.fiche.LigneFraisEnForfaits
      .push(new LigneFraisEnForfait(-1,1,1,this.fiche.id));
  }

  ajouterLigneHorsForfait() {
    this.fiche.LigneFraisHorsForfaits
      .push(new LigneFraisHorsForfait(-1,"",1,0.0,this.fiche.id,null));
  }

  retirerLigneEnForfait(index) {
    let deletedLine = this.fiche.LigneFraisEnForfaits.splice(index, 1);
    this.ligneForfaitSuprimees.push(deletedLine[0].id);
  }
  retirerLigneHorsForfait(index) {
    let deletedLine = this.fiche.LigneFraisHorsForfaits.splice(index, 1);
    this.ligneHorsForfaitSuprimees.push(deletedLine[0].id);
  }

  confirmerFiche() {
    try {
      // ---- Partie mois et annee
      let payload = {
        mois: this.fiche.mois.toString().padStart(2, '0'),
        annee: this.fiche.annee.toString()
      };
      this.fichesService.updateFiche(this.fiche.id, payload).subscribe();

      // ---- Partie Ligne En Forfait ----
      // delete en BDD des lignes suprimées
      this.ligneForfaitSuprimees.map(id=>{
        this.ligneFraisEnForfaitsService.deleteLigne(id).subscribe();
      })
      // creation et update
      if (!!this.ligneForfaitComponents){
        this.ligneForfaitComponents.map(item => {
          let {id, ...payload} = item.ligneData;
          if (id === -1) { // si l'id = -1, c'est une nouvelle ligne => creation
            this.ligneFraisEnForfaitsService.createLigne(payload).subscribe();
          } else { // sinon update
            this.ligneFraisEnForfaitsService.updateLigne(id, payload).subscribe();
          }
        });
      }

      // ---- Partie Ligne Hors Forfait ----
      // delete en BDD des lignes suprimées
      this.ligneHorsForfaitSuprimees.map(id=>{
        this.ligneFraisHorsForfaitsService.deleteLigne(id).subscribe();
      })
      // creation et update
      if (!!this.ligneHorsForfaitComponents){
        this.ligneHorsForfaitComponents.map(item => {
          let {id, ...payload} = item.ligneData;
          if (id === -1) { // si l'id = -1, c'est une nouvelle ligne => creation
            this.ligneFraisHorsForfaitsService.createLigne(payload).subscribe();
          } else { // sinon update
            this.ligneFraisHorsForfaitsService.updateLigne(id, payload).subscribe();
          }
        });
      }
      this.toastr.success("Modification effectuée", "Succès!");
      this.router.navigateByUrl('fiches/index')
    } catch (error) {
      this.toastr.error("Une erreur est survenue", "Oups!");
    }
  }
}

export class NgbdAccordionStatic {
}
