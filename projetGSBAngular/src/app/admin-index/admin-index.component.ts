import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

import { UtilisateursService } from '../services/utilisateur.service';
import { Utilisateur } from '../classes/utilisateur.class';
import { FraisEnForfaitService } from '../services/fraisEnForfait.service';
import { FraisEnForfait } from '../classes/fraisEnForfait.class';

@Component({
  selector: 'app-admin-index',
  templateUrl: './admin-index.component.html',
  styleUrls: ['./admin-index.component.scss']
})
export class AdminIndexComponent implements OnInit {
  utilisateurs = new Array<Utilisateur>();
  utilisateursAffiches = new Array<Utilisateur>();
  listeFraisEnForfait = new Array<FraisEnForfait>();

  constructor(
    private utilisateursService: UtilisateursService,
    private fraisEnForfaitService: FraisEnForfaitService,
    private cookieService: CookieService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.utilisateursService.getUtilisateurs()
      .subscribe(response => {
        this.utilisateurs = response.map(item => {
          return new Utilisateur(
            item.id,
            item.nom,
            item.prenom,
            item.dateEmbauche,
            item.roleId,
            item.identifiant,
            item.motDePasse,
            item.email,
            item.dateNaissance,
            item.adresse,
            item.codePostal,
            item.ville,
          );
        });
      this.utilisateursAffiches = this.utilisateurs;
    });
    this.fraisEnForfaitService.getAllFraisEnForfait()
      .subscribe(response => {
        this.listeFraisEnForfait = response.map(item => {
          return new FraisEnForfait(
            item.id,
            item.libelle,
            item.prixUnitaire
          )
        })
      });
  }

  filtrerListe(event: any) {
    this.utilisateursAffiches = this.utilisateurs.filter((item) => {
      return (item.roleId == event.target.value || event.target.value == -1);
    })
  }

  creerUtilisateur() {
    this.router.navigateByUrl('utilisateurs/create');
  }

  creerFrais() {
    this.router.navigateByUrl('frais/index');
  }

}
