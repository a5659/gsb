import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout-button',
  templateUrl: './logout-button.component.html',
  styleUrls: ['./logout-button.component.scss']
})
export class LogoutButtonComponent implements OnInit {
  title = 'AwesomeGSB'; /* Ici, on a le nom du projet, c'est ce qui va apparaitre dans l'onglet du navigateur */
  LogoGSB3 = "assets/LogoGSB3.png";
  BandeGSB2 = "assets/BandeGSB2.png"
  logOut = "assets/LogOut.png"
  constructor(
    private cookieService: CookieService,
    private router: Router,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
  }

  logout() {
    this.cookieService.deleteAll();
    this.toastr.success("Deconnecté avec succès", "Au revoir");
    this.router.navigateByUrl('/');

  }

}
