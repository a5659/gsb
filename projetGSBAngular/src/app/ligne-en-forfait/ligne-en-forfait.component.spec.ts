import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LigneEnForfaitComponent } from './ligne-en-forfait.component';

describe('LigneEnForfaitComponent', () => {
  let component: LigneEnForfaitComponent;
  let fixture: ComponentFixture<LigneEnForfaitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LigneEnForfaitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LigneEnForfaitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
