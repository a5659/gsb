import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FraisEnForfait } from '../classes/fraisEnForfait.class';
import { FraisEnForfaitService } from '../services/fraisEnForfait.service';

@Component({
  selector: '[app-ligne-en-forfait]',
  templateUrl: './ligne-en-forfait.component.html',
  styleUrls: ['./ligne-en-forfait.component.scss']
})
export class LigneEnForfaitComponent implements OnInit {
  TrashIcon = "assets/Trash.png";
  @Input() ligneData: any = {};
  @Input() verrouillee: boolean = false;
  @Input() index: number = 0;

  listeFraisEnForfait = new Array<FraisEnForfait>();
  fraisId: number = 0;
  prixUnitaire: number = 0;
  quantite: number = 0;
  prixTotal: number = 0;

  @Output() supprimer: EventEmitter<number> = new EventEmitter<number>();

  constructor(
    private fraisEnForfaitService: FraisEnForfaitService
  ) { }

  ngOnInit(): void {
    this.fraisEnForfaitService.getAllFraisEnForfait()
      .subscribe(response => {
        this.listeFraisEnForfait = response.map(item => {
          return new FraisEnForfait(
            item.id,
            item.libelle,
            item.prixUnitaire
          )
        })
        this.calculPrix();
      });
  }

  changeFrais(event) {
    this.ligneData.fraisEnForfaitId = parseInt(event.target.value);
    this.calculPrix();
  }

  calculPrix() {
    if (this.ligneData.fraisEnForfaitId != 0) {
      this.prixUnitaire = this.listeFraisEnForfait[this.ligneData.fraisEnForfaitId-1].prixUnitaire; //-1 car les id commencent à 1 en BDD
    }

    this.prixTotal = this.prixUnitaire * this.ligneData.quantite;
  }

  supprimerLigne() {
    this.supprimer.emit(this.index);
  }
}
