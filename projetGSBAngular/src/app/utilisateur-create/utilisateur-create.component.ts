import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

import { UtilisateursService } from '../services/utilisateur.service';
import { Utilisateur } from '../classes/utilisateur.class';

@Component({
  selector: 'app-utilisateur-create',
  templateUrl: './utilisateur-create.component.html',
  styleUrls: ['./utilisateur-create.component.scss']
})
export class UtilisateurCreateComponent implements OnInit {
  utilisateur: Utilisateur = new Utilisateur(-1, "", "", new Date(), 1, "", "");
  dateEmbaucheJSON: any = {year: 2022, month: 1, day: 1};
  dateNaissanceJSON?: any = null;

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private utilisateursService: UtilisateursService,
  ) { }

  ngOnInit(): void {

  }

  confirmerUtilisateur() {
    if (!this.utilisateur.identifiant || !this.utilisateur.motDePasse){
      this.toastr.error("Veuillez fournir un identifiant et un mot de passe", "Champs requis");
      return;
    }
    if (!this.utilisateur.nom || !this.utilisateur.prenom){
      this.toastr.error("Veuillez fournir un nom et un prénom", "Champ requis!");
      return;
    }
    this.utilisateur.dateEmbauche = new Date(`${this.dateEmbaucheJSON['year']}-${this.dateEmbaucheJSON['month']}-${this.dateEmbaucheJSON['day']}`);
    if (!!this.dateNaissanceJSON) {
      this.utilisateur.dateNaissance = new Date(`${this.dateNaissanceJSON['year']}-${this.dateNaissanceJSON['month']}-${this.dateNaissanceJSON['day']}`);
    }
    let {id, ...payload} = this.utilisateur;
    this.utilisateursService.createUtilisateur(payload)
      .subscribe(data => {
        this.toastr.success("L'utilisateur a été créé", "Succès!");
        this.router.navigateByUrl('admin/index')

      },
      error => {
        this.toastr.error("Une erreur est survenue", "Oups!");
      });
  }
}
