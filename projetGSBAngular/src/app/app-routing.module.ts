import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { AppComponent } from './app.component';
import { RendezVousComponent } from './rendez-vous/rendez-vous.component';
import { LabosComponent } from './labos/labos.component';
import { MapComponent } from './map/map.component';
import { PatientAccessComponent } from './patient-access/patient-access.component';
import { ProAccessComponent } from './pro-access/pro-access.component';
import { FormulaireContactComponent } from './formulaire-contact/formulaire-contact.component';

import { FicheIndexComponent } from './fiche-index/fiche-index.component';
import { FicheCreateComponent } from './fiche-create/fiche-create.component';
import { FicheEditComponent } from './fiche-edit/fiche-edit.component';
import { FicheIndexComptableComponent } from './fiche-index-comptable/fiche-index-comptable.component';
import { FicheEditComptableComponent } from './fiche-edit-comptable/fiche-edit-comptable.component';
import { FraisIndexComponent } from './frais-index/frais-index.component';
import { AdminIndexComponent } from './admin-index/admin-index.component';
import { UtilisateurCreateComponent } from './utilisateur-create/utilisateur-create.component';
import { UtilisateurEditComponent } from './utilisateur-edit/utilisateur-edit.component';


const routes: Routes = [
  {path: 'rdv', component: RendezVousComponent},
  {path: 'labos', component: LabosComponent},
  {path: 'patient-access', component: PatientAccessComponent},
  {path: 'pro-access', component: ProAccessComponent},
  {path: 'formulaire-contact', component: FormulaireContactComponent},

  { path: "fiches/index", component: FicheIndexComponent },
  { path: "fiches/create", component: FicheCreateComponent },
  { path: "fiches/edit/:id", component: FicheEditComponent },
  { path: "fiches/index-comptable", component: FicheIndexComptableComponent },
  { path: "fiches/edit-comptable/:id", component: FicheEditComptableComponent },
  { path: "frais/index", component: FraisIndexComponent },
  { path: "admin/index", component: AdminIndexComponent },
  { path: "utilisateurs/create", component: UtilisateurCreateComponent },
  { path: "utilisateurs/edit/:id", component: UtilisateurEditComponent },

  {path: '', component: AccueilComponent},
  {path: '**', redirectTo: '', pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
