import { Component, OnInit, Input } from '@angular/core';

import { FraisEnForfait } from '../classes/fraisEnForfait.class';

@Component({
  selector: '[app-frais-en-forfait-form]',
  templateUrl: './frais-en-forfait-form.component.html',
  styleUrls: ['./frais-en-forfait-form.component.scss']
})
export class FraisEnForfaitFormComponent implements OnInit {
  @Input() fraisData: FraisEnForfait = new FraisEnForfait(-1,"",0);
  @Input() index: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

}
