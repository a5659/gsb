import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FraisEnForfaitFormComponent } from './frais-en-forfait-form.component';

describe('FraisEnForfaitFormComponent', () => {
  let component: FraisEnForfaitFormComponent;
  let fixture: ComponentFixture<FraisEnForfaitFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FraisEnForfaitFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FraisEnForfaitFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
