import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
recherche : string = '';
title = 'AwesomeGSB'; /* Ici, on a le nom du projet, c'est ce qui va apparaitre dans l'onglet du navigateur */
LogoGSB3 = "assets/LogoGSB3.png";
BandeGSB2 = "assets/BandeGSB2.png";
Loupe = "assets/Loupe.png"

  constructor() { }

  ngOnInit(): void {
  }

}
