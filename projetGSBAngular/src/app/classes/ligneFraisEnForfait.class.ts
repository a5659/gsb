export class LigneFraisEnForfait {

  constructor(
    public id: number,
    public quantite: number,
    public fraisEnForfaitId: number,
    public ficheFraisId: number
  ) {}

}
