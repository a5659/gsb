export class FraisEnForfait {

  constructor(
    public id: number,
    public libelle: string,
    public prixUnitaire: number,
  ) {}

}
