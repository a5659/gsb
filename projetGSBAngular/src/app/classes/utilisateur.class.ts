export class Utilisateur {
  constructor(
    public id: number,
    public nom: string,
    public prenom: string,
    public dateEmbauche: any,
    public roleId: number,
    public identifiant: string,
    public motDePasse: string,
    public email?: string,
    public dateNaissance?: any,
    public adresse?: string,
    public codePostal?: string,
    public ville?: string
  ) {}

}
