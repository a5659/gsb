export class LigneFraisHorsForfait {

  constructor(
    public id: number,
    public libelle: string,
    public quantite: number,
    public prixUnitaire: number,
    public ficheFraisId: number,
    public justificatifId?: number|null
  ) {}

}
