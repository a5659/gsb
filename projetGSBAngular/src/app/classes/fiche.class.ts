import { LigneFraisEnForfait } from '../classes/ligneFraisEnForfait.class';
import { LigneFraisHorsForfait } from '../classes/ligneFraisHorsForfait.class';

export class Fiche {
  constructor(
    public id: number,
    public mois: string,
    public annee: string,
    public etat: number,
    public LigneFraisEnForfaits: Array<LigneFraisEnForfait>,
    public LigneFraisHorsForfaits: Array<LigneFraisHorsForfait>,
    public utilisateurId: number,
    public dateModification: Date,
  ) {}

  getStatusLabel(status: number) {
    var labels = ['archivée', 'validée', 'envoyée', 'brouillon']
    return labels[status - 1]
  }
}
