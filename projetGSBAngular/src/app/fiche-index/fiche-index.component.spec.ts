import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheIndexComponent } from './fiche-index.component';

describe('FicheIndexComponent', () => {
  let component: FicheIndexComponent;
  let fixture: ComponentFixture<FicheIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FicheIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
