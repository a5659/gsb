import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

import { FichesService } from '../services/fiche.service';
import { Fiche } from '../classes/fiche.class';

@Component({
  selector: 'app-fiche-index',
  templateUrl: './fiche-index.component.html',
  styleUrls: ['./fiche-index.component.scss']
})
export class FicheIndexComponent implements OnInit {

  fiches = new Array<Fiche>();
  title = 'AwesomeGSB'; /* Ici, on a le nom du projet, c'est ce qui va apparaitre dans l'onglet du navigateur */
  LogoGSB3 = "assets/LogoGSB3.png";
  BandeGSB2 = "assets/BandeGSB2.png"

  constructor(
    private fichesService: FichesService,
    private cookieService: CookieService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    var utilisateurId = parseInt(this.cookieService.get('utilisateurId'));

    this.fichesService.getFichesByVisiteurId(utilisateurId)
      .subscribe(response => {
        this.fiches = response.map(item => {
          {
            return new Fiche(
              item.id,
              item.mois,
              item.annee,
              item.etat,
              item.LigneFraisEnForfaits,
              item.LigneFraisHorsForfaits,
              item.utilisateurId,
              item.dateModification
            )
          }
        })
    })
  }

  creerFiche() {
    this.router.navigateByUrl('fiches/create')
  }

  envoyerFiche(index) {
    let fiche = this.fiches[index];
    fiche.etat = 3;
    this.fichesService.updateFiche(fiche.id, {etat: 3}).subscribe();
  }
}
