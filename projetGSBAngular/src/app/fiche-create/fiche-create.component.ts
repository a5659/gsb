import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import { FichesService } from '../services/fiche.service';
import { LigneFraisEnForfaitsService } from '../services/ligneFraisEnForfait.service';
import { LigneFraisHorsForfaitsService } from '../services/ligneFraisHorsForfait.service';
import { LigneFraisEnForfait } from '../classes/ligneFraisEnForfait.class';
import { LigneFraisHorsForfait } from '../classes/ligneFraisHorsForfait.class';
import { LigneEnForfaitComponent } from '../ligne-en-forfait/ligne-en-forfait.component';
import { LigneHorsForfaitComponent } from '../ligne-hors-forfait/ligne-hors-forfait.component';

@Component({
  selector: 'app-fiche-create',
  templateUrl: './fiche-create.component.html',
  styleUrls: ['./fiche-create.component.scss']
})
export class FicheCreateComponent implements OnInit {
  title = 'AwesomeGSB'; /* Ici, on a le nom du projet, c'est ce qui va apparaitre dans l'onglet du navigateur */
  LogoGSB3 = "assets/LogoGSB3.png";
  BandeGSB2 = "assets/BandeGSB2.png"

  ficheData: Record<string,any> = {mois: 1, annee: 2022};
  ligneFraisEnForfaits: Array<LigneFraisEnForfait> = [];
  ligneFraisHorsForfaits: Array<LigneFraisHorsForfait> = [];

  @ViewChildren('ligneForfait') ligneForfaitComponents?:QueryList<LigneEnForfaitComponent>;
  @ViewChildren('ligneHorsForfait') ligneHorsForfaitComponents?:QueryList<LigneHorsForfaitComponent>;

  constructor(
    private fichesService: FichesService,
    private ligneFraisEnForfaitsService: LigneFraisEnForfaitsService,
    private ligneFraisHorsForfaitsService: LigneFraisHorsForfaitsService,
    private cookieService: CookieService,
    private toastr: ToastrService,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  ajouterLigneEnForfait() {
    this.ligneFraisEnForfaits.push(new LigneFraisEnForfait(-1,1,1,-1));
  }

  ajouterLigneHorsForfait() {
    this.ligneFraisHorsForfaits.push(new LigneFraisHorsForfait(-1,"",1,0.0,-1,null));
  }

  retirerLigneEnForfait(index) {
    this.ligneFraisEnForfaits.splice(index, 1);
  }
  retirerLigneHorsForfait(index) {
    this.ligneFraisHorsForfaits.splice(index, 1);
  }

  confirmerFiche() {
    try {
      let utilisateurId = parseInt(this.cookieService.get('utilisateurId'));
      this.ficheData['mois'] = this.ficheData['mois'].toString().padStart(2, '0')
      let payload = { utilisateurId, etat: 4, ...this.ficheData };

      this.fichesService.createFiche(payload)
        .subscribe(data => {
          if (!!this.ligneForfaitComponents){
            this.ligneForfaitComponents.map(item => {
              let {id, ...payload} = item.ligneData;
              payload['ficheFraisId'] = data.id;
              this.ligneFraisEnForfaitsService.createLigne(payload).subscribe();
            });
          }
          if (!!this.ligneHorsForfaitComponents){
            this.ligneHorsForfaitComponents.map(item => {
              let {id, ...payload} = item.ligneData;
              payload['ficheFraisId'] = data.id;
              this.ligneFraisHorsForfaitsService.createLigne(payload).subscribe();
            });
          }
        });

      this.toastr.success("La fiche a été créée", "Succès!");
      this.router.navigateByUrl('fiches/index')
    } catch (error) {
      this.toastr.error("Une erreur est survenue", "Oups!");
    }
  }

}
