import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheCreateComponent } from './fiche-create.component';

describe('FicheCreateComponent', () => {
  let component: FicheCreateComponent;
  let fixture: ComponentFixture<FicheCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [  ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
