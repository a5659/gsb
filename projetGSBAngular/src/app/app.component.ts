import { Component } from '@angular/core';

@Component({
  selector: 'app-root', /* ici on a le nom de la balise de redirection */
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'] 
})

export class AppComponent {
  title = 'projetGSBAngular'; /* Ici, on a le nom du projet, c'est ce qui va apparaitre dans l'onglet du navigateur */
}
