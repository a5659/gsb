import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

import { FichesService } from '../services/fiche.service';
import { Fiche } from '../classes/fiche.class';
import { LigneFraisEnForfait } from '../classes/ligneFraisEnForfait.class';
import { LigneFraisHorsForfait } from '../classes/ligneFraisHorsForfait.class';

@Component({
  selector: 'app-fiche-edit-comptable',
  templateUrl: './fiche-edit-comptable.component.html',
  styleUrls: ['./fiche-edit-comptable.component.scss']
})
export class FicheEditComptableComponent implements OnInit {
  ficheId: any;
  fiche: any = {};

  constructor(
    private route: ActivatedRoute,
    private fichesService: FichesService,
    private toastr: ToastrService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(_ => {
      this.ficheId = this.route.snapshot.paramMap.get('id');
    });

    this.fichesService.getFiche(this.ficheId)
      .subscribe(response => {
        this.fiche = new Fiche(
          response.id,
          response.mois,
          response.annee,
          response.etat,
          response.LigneFraisEnForfaits.map(item => {
            return new LigneFraisEnForfait(
              item.id,
              item.quantite,
              item.fraisEnForfaitId,
              item.ficheFraisId
            )
          }),
          response.LigneFraisHorsForfaits.map(item => {
            return new LigneFraisHorsForfait(
              item.id,
              item.libelle,
              item.quantite,
              item.prixUnitaire,
              item.ficheFraisId,
              item.justificatifId,
            )
          }),
          response.utilisateurId,
          response.dateModification
        )
    })
  }

  changerEtat(nouvelEtat){
    this.fichesService.updateFiche(this.ficheId, {etat: nouvelEtat}).subscribe(
      data => {
        this.toastr.success("Modification effectuée", "Succès!");
      },
      error => {
        this.toastr.error("Une erreur est survenue", "Oups!");
      }
    )
    this.router.navigateByUrl('fiches/index-comptable');
  }

}
