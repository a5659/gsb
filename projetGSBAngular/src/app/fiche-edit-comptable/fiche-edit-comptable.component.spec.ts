import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheEditComptableComponent } from './fiche-edit-comptable.component';

describe('FicheEditComptableComponent', () => {
  let component: FicheEditComptableComponent;
  let fixture: ComponentFixture<FicheEditComptableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FicheEditComptableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheEditComptableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
