import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import jwt_decode from "jwt-decode";

import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-pro-access',
  templateUrl: './pro-access.component.html',
  styleUrls: ['./pro-access.component.scss'],
  styles: [`.form-control { width: 300px; }`]
})

export class ProAccessComponent implements OnInit {

  title = 'AwesomeGSB'; /* Ici, on a le nom du projet, c'est ce qui va apparaitre dans l'onglet du navigateur */
  LogoGSB = "assets/LogoGSB3.jpg";

  identifiant: string = "";
  motDePasse: string = "";

  public model: any;
  constructor(
    private authService: AuthService,
    private cookieService: CookieService,
    private router: Router,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void { }

  login() {
    this.authService.login(this.identifiant, this.motDePasse)
      .subscribe(
        (data: any) => {
          let decoded: Record<any,any> =  jwt_decode(data.token);
          this.cookieService.set('utilisateurId', decoded['utilisateurId']);
          this.cookieService.set('roleId', decoded['roleId']);
          this.cookieService.set('accessToken', data.token);

          // gestion de la redirection
          let path: string = "";
          switch(decoded['roleId']){
            case 1:
              path = "fiches/index";
              break;
            case 2:
              path = "fiches/index-comptable";
              break;
            case 3:
              path = "admin/index";
              break;
          }
          this.toastr.success("Connecté avec succès", "Bienvenue!");
          this.router.navigateByUrl(path);
        },
        (error) => {
          this.toastr.error("Connexion refusée.");
        }
      )
  }

}


