import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProAccessComponent } from './pro-access.component';

describe('ProAccessComponent', () => {
  let component: ProAccessComponent;
  let fixture: ComponentFixture<ProAccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProAccessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
