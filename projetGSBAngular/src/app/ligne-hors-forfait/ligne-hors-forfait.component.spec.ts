import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LigneHorsForfaitComponent } from './ligne-hors-forfait.component';

describe('LigneHorsForfaitComponent', () => {
  let component: LigneHorsForfaitComponent;
  let fixture: ComponentFixture<LigneHorsForfaitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LigneHorsForfaitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LigneHorsForfaitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
