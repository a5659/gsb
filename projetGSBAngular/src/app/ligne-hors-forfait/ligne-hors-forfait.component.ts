import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: '[app-ligne-hors-forfait]',
  templateUrl: './ligne-hors-forfait.component.html',
  styleUrls: ['./ligne-hors-forfait.component.scss']
})
export class LigneHorsForfaitComponent implements OnInit {
  TrashIcon = "assets/Trash.png";
  @Input() ligneData: any;
  @Input() verrouillee: boolean = false;
  @Input() index: number = 0;
  prixTotal: number = 0;

  @Output() supprimer: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
    this.calculPrix();
  }


  changePrixUnitaire(event) {
    this.ligneData.prixUnitaire = parseInt(event.target.value);
    this.calculPrix();
  }

  changeQuantite(event) {
    this.ligneData.quantite = parseInt(event.target.value);
    this.calculPrix();
  }

  calculPrix() {
    this.prixTotal = this.ligneData.prixUnitaire * this.ligneData.quantite;
  }

  supprimerLigne() {
    this.supprimer.emit(this.index);
  }
}
