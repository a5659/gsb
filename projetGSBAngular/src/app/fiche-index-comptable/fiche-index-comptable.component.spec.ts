import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheIndexComptableComponent } from './fiche-index-comptable.component';

describe('FicheIndexComptableComponent', () => {
  let component: FicheIndexComptableComponent;
  let fixture: ComponentFixture<FicheIndexComptableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FicheIndexComptableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheIndexComptableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
