import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

import { FichesService } from '../services/fiche.service';
import { Fiche } from '../classes/fiche.class';
import { Utilisateur } from '../classes/utilisateur.class';
import { UtilisateursService } from '../services/utilisateur.service';

@Component({
  selector: 'app-fiche-index-comptable',
  templateUrl: './fiche-index-comptable.component.html',
  styleUrls: ['./fiche-index-comptable.component.scss']
})
export class FicheIndexComptableComponent implements OnInit {
  visiteurs:Array<Utilisateur> = [];
  visiteurId: number = 0;
  fiches: Record<number,Array<Fiche>> = {};
  title = 'AwesomeGSB'; /* Ici, on a le nom du projet, c'est ce qui va apparaitre dans l'onglet du navigateur */
  LogoGSB3 = "assets/LogoGSB3.png";
  BandeGSB2 = "assets/BandeGSB2.png"

  constructor(
    private cookieService: CookieService,
    private router: Router,
    private fichesService: FichesService,
    private utilisateursService: UtilisateursService,
  ) { }

  ngOnInit(): void {
    // chargement de tous les visiteurs
    this.utilisateursService.getVisiteurs()
      .subscribe(visiteurResponse => {
        // pour chaque visiteur recuperé de la BDD...
        this.visiteurs = visiteurResponse.map(visiteur => {

          // recupere les fiches du visiteur
          this.chargerFiches(visiteur.id);

          return new Utilisateur(
            visiteur.id,
            visiteur.nom,
            visiteur.prenom,
            visiteur.dateEmbauche,
            visiteur.roleId,
            visiteur.identifiant,
            visiteur.motDePasse,
            visiteur.email,
            visiteur.dateNaissance,
            visiteur.adresse,
            visiteur.codePostal,
            visiteur.ville,
          )
        })

        if (this.visiteurs.length !== 0){
          this.visiteurId = this.visiteurs[0].id;
        }
      })

  }

  chargerFiches(visiteurId) {
    // initialisation fiches d'index = visiteurId
    this.fiches[visiteurId] = new Array<Fiche>();

    this.fichesService.getFichesByVisiteurId(visiteurId).subscribe(ficheResponse => {
      // pour chaque fiche obtenue, on en ajoute un clone à fiches[visiteurId]
      ficheResponse.map(fiche => {
        this.fiches[visiteurId].push(
          new Fiche(
            fiche.id,
            fiche.mois,
            fiche.annee,
            fiche.etat,
            fiche.LigneFraisEnForfaits,
            fiche.LigneFraisHorsForfaits,
            fiche.utilisateurId,
            fiche.dateModification
          )
        )
      })
    })

  }

  changeVisiteur(event) {
    this.visiteurId = parseInt(event.target.value);
  }

  listeDeFiches() {
    return (!!this.visiteurId) ? this.fiches[this.visiteurId] : []
  }

}
