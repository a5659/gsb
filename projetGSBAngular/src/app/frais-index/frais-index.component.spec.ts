import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FraisIndexComponent } from './frais-index.component';

describe('FraisIndexComponent', () => {
  let component: FraisIndexComponent;
  let fixture: ComponentFixture<FraisIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FraisIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FraisIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
