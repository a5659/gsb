import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

import { FraisEnForfait } from '../classes/fraisEnForfait.class';
import { FraisEnForfaitService } from '../services/fraisEnForfait.service';
import { FraisEnForfaitFormComponent } from '../frais-en-forfait-form/frais-en-forfait-form.component';

@Component({
  selector: 'app-frais-index',
  templateUrl: './frais-index.component.html',
  styleUrls: ['./frais-index.component.scss']
})
export class FraisIndexComponent implements OnInit {
  listeFraisEnForfait = new Array<FraisEnForfait>();

  @ViewChildren('fraisEnForfait') fraisEnForfaitComponents?:QueryList<FraisEnForfaitFormComponent>;

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private fraisEnForfaitService: FraisEnForfaitService
  ) { }

  ngOnInit(): void {
    this.fraisEnForfaitService.getAllFraisEnForfait()
      .subscribe(response => {
        this.listeFraisEnForfait = response.map(item => {
          return new FraisEnForfait(
            item.id,
            item.libelle,
            item.prixUnitaire
          )
        })
      });
  }

  creerFraisEnForfait() {
    this.listeFraisEnForfait.push(new FraisEnForfait(-1,"",0));
  }

  confirmerListe() {
    //map sur fraisEnForfaitComponents
    if (!!this.fraisEnForfaitComponents){
      this.fraisEnForfaitComponents.map(item => {
        let { id, ...payload } = item.fraisData;
        // si id = -1: nouveau donc create
        if (id === -1) {
          this.fraisEnForfaitService.createFraisEnForfait(payload)
            .subscribe(
              data=>{},
              error=>{
                this.toastr.error("Une erreur est survenue", "Oups!");
                this.router.navigateByUrl('admin/index');
              }
            );
        // si id != -1: pas nouveau donc update
        } else {
          this.fraisEnForfaitService.updateFraisEnForfait(id, payload)
            .subscribe(
              data=>{},
              error=>{
                this.toastr.error("Une erreur est survenue", "Oups!");
                this.router.navigateByUrl('admin/index');
              }
            );
        }
      })
    }
    this.toastr.success("Modification effectuée", "Succès!");
    this.router.navigateByUrl('admin/index');
  }

}
