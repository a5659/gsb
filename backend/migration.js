// Zone en construction


// Zone de connection à l'ancienne DB et migration
const mysql = require("mysql");
const oldConfig = require("./oldDb.config.js");

const db = require("./models/db.js");


const oldConnection = mysql.createConnection({
  host: oldConfig.HOST,
  user: oldConfig.USER,
  password: oldConfig.PASSWORD,
  database: oldConfig.DB,
  port: oldConfig.PORT
});
oldConnection.connect(error => {
  if (error) throw error;
  console.log("Connecté en plus à l'ancienne BDD");
});

/* OK mais buggy hors appel par un controller
const Utilisateur = require("./models/utilisateur.model.js");
const res = Utilisateur.getAll();
console.log(res);
*/

/*  Migration des frais_en_forfait
const Utilisateur = require("./models/utilisateur.model.js");
oldConnection.query("SELECT * FROM Visiteur", function (err, result) {
  if (err) throw err;
  result.forEach(old => {
    let formattedUtilisateur = new Utilisateur({
      nom: old.nom,
      prenom: old.prenom,
      email: null,
      identifiant: old.login,
      motDePasse: old.mdp,
      dateNaissance: null,
      dateEmbauche: old.dateEmbauche,
      ville: old.ville,
      codePostal: old.cp,
      adresse: old.adresse,
      roleId: 6
    });
    db.query("INSERT INTO `utilisateurs` SET ?", formattedUtilisateur, (err, res) => {
      if (err) throw err;
      console.log(res);
    })
  })
});

/*  Migration des frais_en_forfait
const FraisEnForfait = require("./models/fraisEnForfait.model.js");
oldConnection.query("SELECT * FROM FraisForfait", function (err, result) {
  if (err) throw err;
  result.forEach(old => {
    let formattedFraisEnForfait = new FraisEnForfait({
      libelle: old.libelle,
      prixUnitaire: old.montant
    });
    db.query("INSERT INTO `frais_en_forfait` SET ?", formattedFraisEnForfait, (err, result) => {
      if (err) throw err;
      console.log(res);
    })
  })
});
*/
