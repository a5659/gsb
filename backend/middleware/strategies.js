const jwt = require("jsonwebtoken");

// recupération de la config. Pour l'instant, juste une clé de test
const KEY = 'z5dGV9ns1z';

// creation de la méthode de verification de la validité d'un token
const strategies = {

  hasValidToken: (req, res, next) => {
    // recuperation du token, qu'il soit dans le corps, la propriété 'token' ou le header de la requete
    const token = req.body.token || req.query.token || req.headers["x-access-token"];
    // si malgré tout on n'a pas de token, retourne code 403 ('Forbidden')
    if (!token) {
      return res.status(403).send("Un token est requis pour s'authentifier.");
    }
    try {
      // decode le token avec la methode de la librairie jwt, en utilisant la clé
      // de chiffrage de la config
      const decoded = jwt.verify(token, KEY);
      // injecte l'utilisateur ainsi décodé depuis le token dans la requete, pour être utilisé plus tard
      req.roleId = decoded.roleId;
    } catch (err) {
      // en cas d'erreur, cela signifie que le token est invalide. On renvoie le
      // code d'erreur 401 (Unautorized)
      return res.status(401).send("Token d'authentification invalide.");
    }
    // si on arrive la, tout s'est bien passé. on passe à l'execution suivante
    return next();
  },

  isAdmin: (req, res, next) => {
    try {
      if (req.roleId != 3) {
        return res.status(403).send("Cette opération n'est autorisée qu'aux administrateurs");
      }
    } catch (err) {
      // en cas d'erreur, cela signifie que le token est invalide. On renvoie le
      // code d'erreur 401 (Unautorized)
      return res.status(401).send("Token d'authentification invalide.");
    }
    // si on arrive la, tout s'est bien passé. on passe à l'execution suivante
    return next();
  },

  isComptable: (req, res, next) => {
    try {
      if (req.roleId != 2) {
        return res.status(403).send("Cette opération n'est autorisée qu'aux comptables");
      }
    } catch (err) {
      // en cas d'erreur, cela signifie que le token est invalide. On renvoie le
      // code d'erreur 401 (Unautorized)
      return res.status(401).send("Token d'authentification invalide.");
    }
    // si on arrive la, tout s'est bien passé. on passe à l'execution suivante
    return next();
  },

  isVisiteur: (req, res, next) => {
    try {
      if (req.roleId != 1) {
        return res.status(403).send("Cette opération n'est autorisée qu'aux visiteurs commerciaux");
      }
    } catch (err) {
      // en cas d'erreur, cela signifie que le token est invalide. On renvoie le
      // code d'erreur 401 (Unautorized)
      return res.status(401).send("Token d'authentification invalide.");
    }
    // si on arrive la, tout s'est bien passé. on passe à l'execution suivante
    return next();
  },

  isVisiteurOrComptable: (req, res, next) => {
    try {
      if (!(req.roleId == 1 || req.utilisateur.roleId == 2)) {
        return res.status(403).send("Cette opération n'est autorisée qu'aux visiteurs commerciaux ou aux comptables");
      }
    } catch (err) {
      // en cas d'erreur, cela signifie que le token est invalide. On renvoie le
      // code d'erreur 401 (Unautorized)
      return res.status(401).send("Token d'authentification invalide.");
    }
    // si on arrive la, tout s'est bien passé. on passe à l'execution suivante
    return next();
  },

} 

// export de la methode verifyToken pour la rendre disponible
module.exports = strategies;

