const cors = require("cors");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(cors());

// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

require("./routes/auth.routes.js")(app);
require("./routes/fiches.routes.js")(app);
require("./routes/utilisateurs.routes.js")(app);
require("./routes/fraisEnForfait.routes.js")(app);
require("./routes/lignesFraisEnForfait.routes.js")(app);
require("./routes/lignesFraisHorsForfait.routes.js")(app);
require("./routes/justificatifs.routes.js")(app);

// simple route
app.get("/test", (req, res) => {
  res.json({ message: "Le serveur est up." });
});

// set port, listen for requests
app.listen(3000, () => {
  console.log("Server is running on port 3000.");
});
