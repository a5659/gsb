module.exports = app => {
    const justificatifs = require("../controllers/justificatifs.controller.js");
  
    // Create a new justificatif
    app.post("/justificatifs", justificatifs.create);
  
    // Retrieve a single Justificatif with justificatifId
    app.get("/justificatifs/:id", justificatifs.findOne);
  
    // Delete a Justificatif with justificatifId
    app.delete("/justificatifs/:id", justificatifs.deleteOne);
  
    // Create a new Customer
    //app.delete("/customers", customers.deleteAll);
};
