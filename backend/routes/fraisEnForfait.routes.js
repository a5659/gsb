const strategies = require("../middleware/strategies.js");
const fraisEnForfait = require("../controllers/fraisEnForfait.controller.js");

module.exports = app => {

  app.post("/fraisEnForfait", [strategies.hasValidToken, strategies.isAdmin], fraisEnForfait.create);

  app.get("/fraisEnForfait", [strategies.hasValidToken], fraisEnForfait.findAll);

  app.get("/fraisEnForfait/:id", [strategies.hasValidToken], fraisEnForfait.findOne);

  app.put("/fraisEnForfait/:id", [strategies.hasValidToken, strategies.isAdmin], fraisEnForfait.update);
  
  app.delete("/fraisEnForfait/:id", [strategies.hasValidToken, strategies.isAdmin], fraisEnForfait.delete);
  
};
