const strategies = require("../middleware/strategies.js");
const auth = require("../controllers/auth.controller.js");

module.exports = app => {

  app.post("/login", auth.login);

  app.post("/authAdmin", [strategies.hasValidToken, strategies.isAdmin], auth.authAdmin);

  app.post("/authComptable", [strategies.hasValidToken, strategies.isComptable], auth.authComptable);

  app.post("/authVisiteur", [strategies.hasValidToken, strategies.isVisiteur], auth.authVisiteur);

};
