const strategies = require("../middleware/strategies.js");
const utilisateurs = require("../controllers/utilisateurs.controller.js");

module.exports = app => {

  app.post("/utilisateurs", [strategies.hasValidToken, strategies.isAdmin], utilisateurs.create);

  app.get("/utilisateurs", [strategies.hasValidToken], utilisateurs.findAll);

  app.get("/visiteurs", [strategies.hasValidToken], utilisateurs.findAllVisitors);

  app.get("/utilisateurs/:id", [strategies.hasValidToken], utilisateurs.findOne);

  app.put("/utilisateurs/:id", [strategies.hasValidToken, strategies.isAdmin], utilisateurs.update);
  
  app.delete("/utilisateurs/:id", [strategies.hasValidToken, strategies.isAdmin], utilisateurs.delete);
  
  };
