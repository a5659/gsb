const strategies = require("../middleware/strategies.js");
const fiches = require("../controllers/fiches.controller.js");

module.exports = app => {

  app.post("/fiches", [strategies.hasValidToken, strategies.isVisiteur], fiches.create);

  app.get("/fiches/visiteur/:utilisateurId", [strategies.hasValidToken, strategies.isVisiteurOrComptable], fiches.findAllByVisitorId)

  app.get("/fiches/:id", [strategies.hasValidToken, strategies.isVisiteurOrComptable], fiches.findOne);

  app.put("/fiches/:id", [strategies.hasValidToken, strategies.isVisiteurOrComptable], fiches.update);
  
  app.delete("/fiches/:id", [strategies.hasValidToken, strategies.isVisiteur], fiches.deleteOne);
  
}
