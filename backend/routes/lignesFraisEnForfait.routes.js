const strategies = require("../middleware/strategies.js");
const lignesFraisEnForfait = require("../controllers/lignesFraisEnForfait.controller.js");

module.exports = app => {
  
  // Create a new ligneFraisEnForfait
  app.post("/lignesFraisEnForfait", [strategies.hasValidToken, strategies.isVisiteur], lignesFraisEnForfait.create);

  // Update a LigneFraisEnForfait with ligneFraisEnForfaitId
  app.put("/lignesFraisEnForfait/:id", [strategies.hasValidToken, strategies.isVisiteur], lignesFraisEnForfait.update);

  // Delete a LigneFraisEnForfait with ligneFraisEnForfaitId
  app.delete("/lignesFraisEnForfait/:id", [strategies.hasValidToken, strategies.isVisiteur], lignesFraisEnForfait.delete);
  
  };
