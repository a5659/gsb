const strategies = require("../middleware/strategies.js");
const lignesFraisHorsForfait = require("../controllers/lignesFraisHorsForfait.controller.js");
module.exports = app => {
  
  app.post("/lignesFraisHorsForfait", [strategies.hasValidToken, strategies.isVisiteur], lignesFraisHorsForfait.create);

  app.put("/lignesFraisHorsForfait/:id", [strategies.hasValidToken, strategies.isVisiteur], lignesFraisHorsForfait.update);

  app.delete("/lignesFraisHorsForfait/:id", [strategies.hasValidToken, strategies.isVisiteur], lignesFraisHorsForfait.delete);
  
  };
