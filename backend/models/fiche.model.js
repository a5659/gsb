const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const LigneFraisEnForfait = require("../models/ligneFraisEnForfait.model.js")
const LigneFraisHorsForfait = require("../models/ligneFraisHorsForfait.model.js")

const Fiche = sequelize.define('Fiche', {
  mois: { type: DataTypes.INTEGER },
  annee: { type: DataTypes.INTEGER },
  dateModification: { type: DataTypes.DATE },
  etat: { type: DataTypes.INTEGER }
},{
  tableName: 'fiches_frais',
  timestamps: false,
});

Fiche.hasMany(LigneFraisEnForfait, {
  foreignKey: 'ficheFraisId'
})
Fiche.hasMany(LigneFraisHorsForfait, {
  foreignKey: 'ficheFraisId'
})

module.exports = Fiche;
