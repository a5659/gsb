const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const LigneFraisEnForfait = require("../models/ligneFraisEnForfait.model.js")
const LigneFraisHorsForfait = require("../models/ligneFraisHorsForfait.model.js")

const Justificatif = sequelize.define('Justificatif', {
  nom: { type: DataTypes.STRING },
  dateUpload: { type: DataTypes.DATE },
},{
  tableName: 'justificatifs',
  timestamps: false,
});


Justificatif.hasMany(LigneFraisHorsForfait, {
  as: 'lignes_frais_hors_forfait',
  foreignKey: 'justificatifId'
})

module.exports = Justificatif;
