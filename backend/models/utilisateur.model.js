const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const Role = require("../models/role.model.js");
const Fiche = require("../models/fiche.model.js");

const Utilisateur = sequelize.define('Utilisateur', {
  nom: { type: DataTypes.STRING },
  prenom: { type: DataTypes.STRING },
  email: { type: DataTypes.STRING },
  identifiant: { type: DataTypes.STRING },
  motDePasse: { type: DataTypes.STRING },
  dateNaissance: { type: DataTypes.DATEONLY },
  dateEmbauche: { type: DataTypes.DATEONLY },
  adresse: { type: DataTypes.STRING },
  codePostal: { type: DataTypes.STRING },
  ville: { type: DataTypes.STRING },
}, {
  tableName: 'utilisateurs',
  timestamps: false,
});

Utilisateur.hasMany(Fiche, {
  foreignKey: 'utilisateurId'
})
Utilisateur.belongsTo(Role, {
  foreignKey: 'roleId'
})

module.exports = Utilisateur;
