const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");
//const sql = require("./db.js");
// constructor
const LigneFraisEnForfait = require("../models/ligneFraisEnForfait.model.js");

const FraisEnForfait = sequelize.define('FraisEnForfait', {
  libelle: { type: DataTypes.STRING},
  prixUnitaire: { type: DataTypes.DECIMAL(10,2) },
},{
  tableName: 'frais_en_forfait',
  timestamps: false,
});


FraisEnForfait.hasMany(LigneFraisEnForfait, {
  as: 'lignes_frais_en_forfait',
  foreignKey: 'fraisEnForfaitId'
})
  
module.exports = FraisEnForfait;
