const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const LigneFraisEnForfait = sequelize.define('LigneFraisEnForfait', {
  quantite: { type: DataTypes.INTEGER },
},{
  tableName: 'lignes_frais_en_forfait',
  timestamps: false,
});

module.exports = LigneFraisEnForfait;
