const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const LigneFraisHorsForfait = sequelize.define('LigneFraisHorsForfait', {
  libelle: { type: DataTypes.STRING },
  quantite: { type: DataTypes.INTEGER },
  prixUnitaire: { type: DataTypes.DECIMAL(10, 2) },
},{
  tableName: 'lignes_frais_hors_forfait',
  timestamps: false,
});

module.exports = LigneFraisHorsForfait;
