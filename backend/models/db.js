const dbConfig = require("../db.config.js");
const { Sequelize } = require('sequelize');

// Create a connection to the database
const connection = new Sequelize(
  dbConfig.DB,
  dbConfig.USER,
  dbConfig.PASSWORD,
  {
    dialect: 'mysql',
    host: dbConfig.HOST,
  },
);

/*
// open the MySQL connection
connection.connect(error => {
  if (error) throw error;
  console.log("Successfully connected to the database.");
});
*/

module.exports = connection;
