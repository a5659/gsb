const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const Fiche = require("../models/fiche.model.js");

const Etat = sequelize.define('Etat', {
  libelle: { type: DataTypes.STRING },
},{
  tableName: 'etats',
  timestamps: false,
});

Etat.hasMany(Fiche, { 
  as: 'fiches_frais',
  foreignKey: 'etat'
});

module.exports = Etat;
