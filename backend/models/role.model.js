const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const Role = sequelize.define('Role', {
  libelle: { type: DataTypes.STRING },
},{
  tableName: 'roles',
  timestamps: false,
});

module.exports = Role;
