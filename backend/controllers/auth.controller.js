const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const Utilisateur = require("../models/utilisateur.model.js");

// recupération de la config. Pour l'instant, juste une clé de test
const KEY = 'z5dGV9ns1z';
//
exports.login = async (req, res) => {
  const { identifiant, motDePasse } = req.body;
  // verification de la presence d'un login et mot de passe dans la requete
  if (!(identifiant && motDePasse)) {
    res.status(400).send("Un login et un mot de passe sont requis!");
    return
  }
  // recuperation d'un utilisateur par credentials
  let data = await Utilisateur.findOne({where: { identifiant: identifiant } });
  if (!data) {
      res.status(404).send({message: 'Aucun utilisateur ne correspond aux identifiants fournis.'});
  } else {
    const validPassword = await bcrypt.compare(motDePasse, data.motDePasse);
    if (!validPassword) {
      res.status(404).send({
        message: 'Aucun utilisateur ne correspond aux identifiants fournis.'
      });
      return
    } else {
      // créé un token avec jwt.sign . Utilise pour cela les infos qu'on veut
      // stocker dedans, la clé de chiffrage, et les options de création
      // (ici, uniquement une durée de validité)
      const token = jwt.sign(
        {
          utilisateurId: data.id,
          roleId: data.roleId
        },
        KEY,
        {expiresIn: "1h"}
      );
      res.status(200).send( {success: true, message: "Connecté avec succès", token} );
    }
  }
};

/*
  Pour arriver sur l'une de ces trois methodes, les strategies
  d'authentification ont déja vérifié la présence et l'authenticité du token,
  ainsi que le role de l'utilisateur emettant la requete.
  On peut donc se permettre de simplement répondre un status 200
  pour confirmer son niveau d'accès
*/
exports.authAdmin = async (req, res) => {
  res.status(200).send( {success: true, message: "Accès accordé"} );
};

exports.authComptable = async (req, res) => {
  res.status(200).send( {success: true, message: "Accès accordé"} );
};

exports.authVisiteur = async (req, res) => {
  res.status(200).send( {success: true, message: "Accès accordé"} );
};



