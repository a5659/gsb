const Fiche = require("../models/fiche.model.js");
const LigneFraisEnForfait = require("../models/ligneFraisEnForfait.model.js");
const LigneFraisHorsForfait = require("../models/ligneFraisHorsForfait.model.js");
const Etat = require("../models/etat.model.js");

exports.findAllByVisitorId = (req, res) => {
  const utilisateurId = req.params.utilisateurId;
  Fiche.findAll({
    where: { utilisateurId },
    include: [
      { model: LigneFraisEnForfait },
      { model: LigneFraisHorsForfait },
    ]
  })
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Impossible de trouver la Fiches  pour l'utilisateur d'id=${utilisateurId}.`});
        return;
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).send({message: "Erreur serveur en cherchant les Fiches pour l'utilisateur d'id " + utilisateurId});
      return;
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;
  Fiche.findOne({
    where: { id },
    include: [
      { model: LigneFraisEnForfait },
      { model: LigneFraisHorsForfait },
    ]
  })
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Impossible de trouver la Fiche d'id=${id}.` });
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Erreur serveur en cherchant la Fiche d'id " + id});
      return;
    });
};

exports.create = (req, res) => {
  // Vérifie que le corps de la requete n'est pas vide
  if (!req.body) {
    res.status(400).send({message: "Le contenu ne peut pas être vide."});
    return;
    // TODO verifier que les données sont valides
  }

  // appel de fonction create fournie par Sequelize 
  let ficheData = { 
    ...req.body,
    dateModification: new Date().toISOString().slice(0, 19).replace('T', ' ')
    };
  Fiche.create(ficheData)
  // gestion du retour (renvoie la réponse si ok, sinon renvoie un status 500 et
  // l'erreur
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      console.log(err);
      res.status(500).send({message: err.message || "une erreur interne est survenue."});
      return;
    });
}

exports.update = (req, res) => {
  const id = req.params.id;
  // Vérifie que le corps de la requete n'est pas vide
  if (!req.body) {
    res.status(400).send({ message: "Le contenu ne peut pas être vide." });
    return;
  }
  // construit un objet (possiblement partiel) à partir du corps de requete, et
  // ajoute la date actuelle pour dateModification
  let ficheData = { 
    ...req.body,
    dateModification: new Date().toISOString().slice(0, 19).replace('T', ' ')
    };
  // ceci permet de retirer les clés de valeur 'undefined' de l'objet fiche
  ficheData = JSON.parse(JSON.stringify(ficheData));

  // lance la modification en BDD
  Fiche.update(ficheData, { where: { id: id } })
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Impossible de trouver la Fiche d'id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Une erreur interne est survenue."});
      return;
    });
}

exports.deleteOne = (req, res) => {
  const id = req.params.id;
  Fiche.destroy( {where: { id: id } })
    .then(data => {
      if (data) {
        res.sendStatus(200);
      } else {
        res.status(404).send({message: `Impossible de trouver la Fiche d'id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Une erreur interne est survenue."});
      return;
    });
};



