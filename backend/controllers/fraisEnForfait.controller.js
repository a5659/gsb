const FraisEnForfait = require("../models/fraisEnForfait.model.js");

// // Create and Save a new FraisEnForfait
exports.create = (req, res) => {
  //verifie le contenu de la requete
  if (!req.body) {
    res.status(400).send({ message: "Le corps de la requete est vide."});
    return;
  }
  if (!(req.body.libelle && req.body.prixUnitaire)) {
    res.status(400).send({ message: "Certains parametres sont vide."});
    return;
  }
  // crée un objet copie de req.body, retire les valeurs undefined
  let fraisData = req.body;
  fraisData = JSON.parse(JSON.stringify(fraisData));

  // Enregistre en BDD
  FraisEnForfait.create(fraisData)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({ message: err.message || "une erreur interne est survenue." });
      return;
    });
};

// Retrieve all FraisEnForfaits from the database.
exports.findAll = (req, res) => {
  FraisEnForfait.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({ message: err.message || "une erreur interne est survenue." });
      return;
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;
  FraisEnForfait.findOne({ where: {id: id} })
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Impossible de trouver le frais en forfait d'id=${id}.`});
        return;
      }
    })
    .catch(err => {
      console.log(err)
      res.status(500).send({message: "Une erreur interne est survenue."});
      return;
    });
};

exports.update = (req, res) => {
  const id = req.params.id;
  // Vérifie que le corps de la requete n'est pas vide
  if (!req.body) {
    res.status(400).send({message: "Le contenu ne peut pas être vide."});
    return;
  }
  // construit un objet (possiblement partiel) à partir du corps de requete, et
  let fraisData = req.body;
  // ceci permet de retirer les clés de valeur 'undefined' de l'objet fiche
  fraisData = JSON.parse(JSON.stringify(fraisData));

  // lance la modification en BDD
  FraisEnForfait.update(fraisData, { where: { id: id } })
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Impossible de trouver le frais en forfait d'id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Une erreur interne est survenue."});
      return;
    });
}

exports.delete = (req, res) => {
  const id = req.params.id;
  FraisEnForfait.destroy( {where: { id: id } })
    .then(data => {
      if (data) {
        res.sendStatus(200);
      } else {
        res.status(404).send({message: `Impossible de trouver le frais en forfait d'id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Une erreur interne est survenue."});
      return;
    });
};
