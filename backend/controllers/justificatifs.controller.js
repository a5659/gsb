const Justificatif = require("../models/justificatif.model.js");

exports.create = (req, res) => {
  // Vérifie que le corps de la requete n'est pas vide
  if (!req.body) {
    res.status(400).send({message: "Le contenu ne peut pas être vide."});
    return;
    // TODO verifier que les données sont valides
  }

  // appel de fonction create fournie par Sequelize 
  let justificatifData = { 
    ...req.body,
    dateUpload: new Date().toISOString().slice(0, 19).replace('T', ' ')
  };
  Justificatif.create(justificatifData)
  // gestion du retour (renvoie la réponse si ok, sinon renvoie un status 500 et
  // l'erreur
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({message: err.message || "une erreur interne est survenue."});
      return;
    });
}

/*
// Retrieve all Justificatifs from the database.
exports.findAll = (req, res) => {
    Justificatif.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving customers."
            });
        else res.send(data);
    });
};
*/

// Find a single Justificatif with a customerId
exports.findOne = (req, res) => {
  const id = req.params.id;
  Justificatif.findByPk(id)
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Impossible de trouver le Justificatif d'id=${id}.` });
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Erreur serveur en cherchant le Justificatif d'id " + id});
      return;
    });
};

/*
// Update a Justificatif identified by the customerId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    console.log(req.body);
    let justificatif = new Justificatif(req.body);
    justificatif = JSON.parse(JSON.stringify(justificatif));

    Justificatif.updateById(
      req.params.id,
      justificatif,
      (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Pas d'justificatif trouvé avec l'id : ${req.params.id}.`
            });
          } else {
            res.status(500).send({
              message: "Erreur lors de la modification de l'justificatif avec  l'id : " + req.params.id
            });
          }
        } else res.send(data);
      }
    );
  };
*/

// // Delete a Justificatif with the specified customerId in the request
exports.deleteOne = (req, res) => {
  const id = req.params.id;
  Justificatif.destroy( {where: { id: id } })
    .then(data => {
      if (data) {
        res.sendStatus(200);
      } else {
        res.status(404).send({message: `Impossible de trouver le Justificatif d'id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Une erreur interne est survenue."});
      return;
    });
};

// // Delete all Justificatifs from the database.
// exports.deleteAll = (req, res) => {

// };
