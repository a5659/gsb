const bcrypt = require("bcrypt");

const Utilisateur = require("../models/utilisateur.model.js");
const Fiche = require("../models/fiche.model.js");
const LigneFraisEnForfait = require("../models/ligneFraisEnForfait.model.js");
const LigneFraisHorsForfait = require("../models/ligneFraisHorsForfait.model.js");
const Role = require("../models/role.model.js");

// Créé et sauve en BDD un Utilisateur
exports.create = async (req, res) => {
  // Valide la requête
  const body = req.body;
  if (!body) {
    res.status(400).send({ message: "Le corps de la requete est vide."});
    return;
  }
  if (!(body.nom && body.prenom && body.email && body.identifiant && body.motDePasse && body.dateEmbauche || body.roleId)) {
    res.status(400).send({ message: "Certains parametres sont vide."});
    return;
  }
  let utilisateurData = req.body;
  // ceci permet de retirer les clés de valeur 'undefined' de l'objet
  utilisateurData = JSON.parse(JSON.stringify(utilisateurData));

  // Generation du sel et hashage du mot de passe
  const salt = await bcrypt.genSalt(10);
  utilisateurData.motDePasse = await bcrypt.hash(utilisateurData.motDePasse, salt);

  Utilisateur.findOne({ where: { identifiant: utilisateurData.identifiant } })
    .then(data => {
      if (!!data) {
        res.status(409).send({message: "Cet identifiant n'est pas disponible."});
        return;
      } else {
        // Save Utilisateur in the database
        Utilisateur.create(utilisateurData)
          .then(data => {
            res.send(data);
          })
          .catch(err => {
            throw new Error(err);
          });
      }
    })
    .catch(err => {
      res.status(500).send({message: err.message || "Une erreur interne est survenue."});
      return;
    });

};

// Retrieve all Utilisateurs from the database.

exports.findAll = (req, res) => {
  Utilisateur.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({ message: err.message || "une erreur interne est survenue." });
      return;
    });
};

exports.findAllVisitors = (req, res) => {
  Utilisateur.findAll({ where: { roleId: 1 } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({ message: err.message || "une erreur interne est survenue." });
      return;
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;
  Utilisateur.findOne({
    where: {id: id},
    include: [{
      model: Fiche,
      include: [
        { model: LigneFraisEnForfait },
        { model: LigneFraisHorsForfait }
      ],
    },
    {model: Role}
    ]
  })
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Impossible de trouver l'utilisateur d'id=${id}.`});
        return;
      }
    })
    .catch(err => {
      console.log(err)
      res.status(500).send({message: "Une erreur interne est survenue."});
      return;
    });
};

// Update a Utilisateur identified by the customerId in the request
exports.update = async (req, res) => {
  const id = req.params.id;
  // Vérifie que le corps de la requete n'est pas vide
  if (!req.body) {
    res.status(400).send({message: "Le contenu ne peut pas être vide."});
    return;
  }
  // construit un objet (possiblement partiel) à partir du corps de requete, et
  let utilisateurData = req.body;
  // ceci permet de retirer les clés de valeur 'undefined' de l'objet
  utilisateurData = JSON.parse(JSON.stringify(utilisateurData));

  // Si la modification porte sur mot le de passe, hashage
  if ('motDePasse' in utilisateurData) {
    const salt = await bcrypt.genSalt(10);
    utilisateurData.motDePasse = await bcrypt.hash(utilisateurData.motDePasse, salt);
  }

  // lance la modification en BDD
  Utilisateur.update(utilisateurData, { where: { id: id } })
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Impossible de trouver l'utilisateur d'id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Une erreur interne est survenue."});
      return;
    });
}

// Delete a Utilisateur with the specified utilisateurId in the request
exports.delete = (req, res) => {
  const id = req.params.id;
  Utilisateur.destroy( {where: { id: id } })
    .then(data => {
      if (data) {
        res.sendStatus(200);
      } else {
        res.status(404).send({ message: `Impossible de trouver l'utilisateur d'id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Une erreur interne est survenue."});
      return;
    });
};
