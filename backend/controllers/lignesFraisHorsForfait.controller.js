const LigneFraisHorsForfait = require("../models/ligneFraisHorsForfait.model.js");


// // Create and Save a new LigneFraisHorsForfait
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({ message: "Le corps de la requete est vide."});
    return;
  }
  // TODO verifier l'existence de tous les champs requis

  // Create a LigneFraisEnForfait
  const ligneData = req.body; 
  // Save LigneFraisEnForfait in the database
  LigneFraisHorsForfait.create(ligneData)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({ message: err.message || "une erreur interne est survenue." });
      return;
  });
};


// Update a LigneFraisHorsForfait identified by the customerId in the request
exports.update = (req, res) => {

  const id = req.params.id;
  // Vérifie que le corps de la requete n'est pas vide
  if (!req.body) {
    res.status(400).send({message: "Le contenu ne peut pas être vide."});
    return;
  }
  // construit un objet (possiblement partiel) à partir du corps de requete, et
  let ligneData = req.body;
  // ceci permet de retirer les clés de valeur 'undefined' de l'objet fiche
  ligneData = JSON.parse(JSON.stringify(ligneData));

  // lance la modification en BDD
  LigneFraisHorsForfait.update(ligneData, { where: { id: id } })
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Impossible de trouver la ligne de frais hors forfait d'id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Une erreur interne est survenue."});
      return;
    });
}


// // Delete a LigneFraisHorsForfait with the specified customerId in the request
exports.delete = (req, res) => {
  const id = req.params.id;
  LigneFraisHorsForfait.destroy( {where: { id: id } })
    .then(data => {
      if (data) {
        res.sendStatus(200);
      } else {
        res.status(404).send({message: `Impossible de trouver la ligne de frais hors forfait d'id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Une erreur interne est survenue."});
      return;
    });
};

// // Delete all LigneFraisHorsForfait from the database.
// exports.deleteAll = (req, res) => {

// };
