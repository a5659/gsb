# gsb

Une application comptable de gestion de frais

## RUN de l'application

L'application est décomposé entre un front-end en angularJS présent dans `projetGSBAngular` qui doit etre compilé puis servis par un serveur web, un back-end présent dans `backend` utilisant nodeJS, et une base de donnée mysql dont le fichier de structure est présent dans `backend/gsb.sql`.

### Run du backend

```BASH
cd backend
npm install

node server.js
```

Ecoute sur le port 3000 par defaut, la conf de connexion a la base de donnée est dans `db.config.js`

### Compilation d'Angular

Pour compiler les JS d'angular:

```BASH
cd projetGSBAngular
npm install 
ng build
```

Les fichier générés sont disponible dans  `projetGSBAngular/dist/projetGSBAngular/` a placer par la suite a la racine du serveur web

### mysql

```SQL
create database gsb;
```

```BASH
mysql gsb < database/gsb.sql
```

La gestion d'acces a la bdd est a configurer pour correspondre au `db.config.js`.

## Run de l'application mais avec Docker

```BASH
docker-compose up -d
```

### Mise à jour de l'application docker  

```BASH
cd /home/ubuntu/code/gsb
# Arret du serveur
docker-compose down 
# mise a jour du repo
git pull
# si on change de branche
git switch
# 'fin bref vous savez faire du git, prennez votre code a jour

# relance du server + build
docker-compose up --build -d 
#optionnel, voir les logs pour savoir ce qu'il se passe (se quite avec ctrl +c)
docker-compose logs -f
```
