import 'package:flutter/material.dart';
import 'package:flutter_application_gsb/screens/admin/add_user.dart';
import 'package:flutter_application_gsb/screens/admin/dashboard_admin.dart';
import 'package:flutter_application_gsb/screens/admin/home_page_admin.dart';
import 'package:flutter_application_gsb/screens/admin/frais_forfait_admin.dart';
import 'package:flutter_application_gsb/screens/admin/add_frais_en_forfait.dart';
import 'package:flutter_application_gsb/screens/admin/update_frais_en_forfait.dart';
import 'package:flutter_application_gsb/screens/admin/update_user.dart';

import 'package:flutter_application_gsb/screens/common/login.dart';

import 'package:flutter_application_gsb/screens/comptable/fiche_archi_compta.dart';
import 'package:flutter_application_gsb/screens/comptable/fiche_envoye_compta.dart';
import 'package:flutter_application_gsb/screens/comptable/valid_non_rembou_compta.dart';
import 'package:flutter_application_gsb/screens/comptable/home_page_comptable.dart';

import 'package:flutter_application_gsb/screens/utilisateur/fiche_archi_user.dart';
import 'package:flutter_application_gsb/screens/utilisateur/fiche_brouillon_user.dart';
import 'package:flutter_application_gsb/screens/utilisateur/fiche_envoye_util.dart';
import 'package:flutter_application_gsb/screens/utilisateur/mes_infos_util.dart';
import 'package:flutter_application_gsb/screens/utilisateur/nouvelle_fiche_util.dart';
import 'package:flutter_application_gsb/screens/utilisateur/home_page_utilisateur.dart';
import 'package:flutter_application_gsb/screens/utilisateur/update_fiche_util.dart';

class RouteGenerator {
  static Route<dynamic>? generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/login':
        return MaterialPageRoute(builder: (_) => const Login());
      //case '/inactifUser':
      //  return MaterialPageRoute(builder: (_) => const InactifUser());

      case '/admin':
        return MaterialPageRoute(builder: (_) => const HomePageAdmin());
      case '/dashboardadmin':
        return MaterialPageRoute(builder: (_) => const DashBoardAdmin());
      case '/addUser':
        return MaterialPageRoute(builder: (_) => const AddUser());
      case '/updateUser':
        final int arg = settings.arguments as int;
        return MaterialPageRoute(builder: (_) => UpdateUser(id: arg));
      case '/adminfraisforfait':
        return MaterialPageRoute(builder: (_) => const AdminFraisForfait());
      case '/addfraisforfait':
        return MaterialPageRoute(builder: (_) => const AddFraisEnForfait());
      case '/updatefraisforfait':
        final int arg = settings.arguments as int;
        return MaterialPageRoute(builder: (_) => UpdateFraisEnForfait(id: arg));

      case '/comptable':
        return MaterialPageRoute(builder: (_) => const HomePageComptable());
      case '/FicheArchiCompta':
        return MaterialPageRoute(builder: (_) => const FicheArchiComptable());
      case '/FicheEnvoyeCompta':
        return MaterialPageRoute(builder: (_) => const FicheEnvoyeComptable());
      case '/ValidNonRembouCompta':
        return MaterialPageRoute(
            builder: (_) => const ValidNonRembouComptable());

      case '/utilisateur':
        return MaterialPageRoute(builder: (_) => const HomePageUtilisateur());
      case '/FicheArchiUtil':
        return MaterialPageRoute(builder: (_) => const FicheArchiUtilisateur());
      case '/FicheBrouillonUtil':
        return MaterialPageRoute(
            builder: (_) => const FicheBrouillonUtilisateur());
      case '/FicheEnvoyeUtil':
        return MaterialPageRoute(
            builder: (_) => const FicheEnvoyeUtilisateur());
      case '/MesInfosUtil':
        return MaterialPageRoute(builder: (_) => const MesInfosUtilisateur());
      case '/NouvelleFicheUtil':
        return MaterialPageRoute(
            builder: (_) => const NouvelleFicheUtilisateur());
      case '/UpdateFicheUtil':
        final int arg = settings.arguments as int;
        return MaterialPageRoute(builder: (_) => UpdateFiche(id: arg));
    }
    return null;
  }
}
