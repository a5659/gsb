import 'dart:convert';

List<LigneFraisHorsForfait> ligneFraisHorsForfaitFromJson(String str) => List<LigneFraisHorsForfait>.from(json.decode(str).map((x) => LigneFraisHorsForfait.fromJson(x)));

String ligneFraisHorsForfaitToJson(List<LigneFraisHorsForfait> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class LigneFraisHorsForfait {
    LigneFraisHorsForfait({
        required this.id,
        required this.libelle,
        required this.quantite,
        required this.prixUnitaire,
        required this.ficheFraisId,
        this.justificatifId,
    });

    int id;
    String libelle;
    int quantite;
    double prixUnitaire;
    int ficheFraisId;
    dynamic justificatifId;

    factory LigneFraisHorsForfait.fromJson(Map<String, dynamic> json) => LigneFraisHorsForfait(
        id: json["id"],
        libelle: json["libelle"],
        quantite: json["quantite"],
        prixUnitaire: json["prixUnitaire"],
        ficheFraisId: json["ficheFraisId"],
        justificatifId: json["justificatifId"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "libelle": libelle,
        "quantite": quantite,
        "prixUnitaire": prixUnitaire,
        "ficheFraisId": ficheFraisId,
        "justificatifId": justificatifId,
    };
}