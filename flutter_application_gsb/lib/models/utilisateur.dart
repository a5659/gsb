// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

List<Utilisateur> userFromJson(String str) =>
    List<Utilisateur>.from(json.decode(str).map((x) => Utilisateur.fromJson(x)));

String userToJson(List<Utilisateur> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Utilisateur {
  Utilisateur({
    required this.id,
    required this.nom,
    required this.prenom,
    this.email,
    required this.identifiant,
    required this.motDePasse,
    required this.dateNaissance,
    required this.dateEmbauche,
    this.adresse,
    this.codePostal,
    this.ville,
    required this.roleId,
  });

  int id;
  String nom;
  String prenom;
  String? email;
  String identifiant;
  String motDePasse;
  DateTime dateNaissance;
  DateTime dateEmbauche;
  dynamic adresse;
  dynamic codePostal;
  dynamic ville;
  int roleId;

  factory Utilisateur.fromJson(Map<String, dynamic> json) => Utilisateur(
        id: json["id"],
        nom: json["nom"],
        prenom: json["prenom"],
        email: json["email"],
        identifiant: json["identifiant"],
        motDePasse: json["motDePasse"],
        dateNaissance: DateTime.parse(json["dateNaissance"]),
        dateEmbauche: DateTime.parse(json["dateEmbauche"]),
        adresse: json["adresse"],
        codePostal: json["codePostal"],
        ville: json["ville"],
        roleId: json["roleId"],
      );

  Map<String, dynamic> toJson() => {
        "nom": nom,
        "prenom": prenom,
        "email": email,
        "identifiant": identifiant,
        "motDePasse": motDePasse,
        "dateNaissance":
            "${dateNaissance.year.toString().padLeft(4, '0')}-${dateNaissance.month.toString().padLeft(2, '0')}-${dateNaissance.day.toString().padLeft(2, '0')}",
        "dateEmbauche":
            "${dateEmbauche.year.toString().padLeft(4, '0')}-${dateEmbauche.month.toString().padLeft(2, '0')}-${dateEmbauche.day.toString().padLeft(2, '0')}",
        "adresse": adresse,
        "codePostal": codePostal,
        "ville": ville,
        "roleId": roleId,
      };
}
