import 'dart:convert';
import 'ligne_frais_en_forfait.dart';
import 'ligne_frais_hors_forfait.dart';

List<Fiche> ficheFromJson(String str) => List<Fiche>.from(json.decode(str).map((x) => Fiche.fromJson(x)));

String ficheToJson(List<Fiche> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Fiche {
    Fiche({
        required this.id,
        required this.mois,
        required this.annee,
        required this.dateModification,
        required this.utilisateurId,
        required this.etat,
        required this.ligneFraisEnForfaits,
        required this.ligneFraisHorsForfaits,
    });

    int id;
    String mois;
    String annee;
    DateTime dateModification;
    int utilisateurId;
    int etat;
    List<LigneFraisEnForfait> ligneFraisEnForfaits;
    List<LigneFraisHorsForfait> ligneFraisHorsForfaits;

    factory Fiche.fromJson(Map<String, dynamic> json) => Fiche(
        id: json["id"],
        mois: json["mois"],
        annee: json["annee"],
        dateModification: DateTime.parse(json["dateModification"]),
        utilisateurId: json["utilisateurId"],
        etat: json["etat"],
        ligneFraisEnForfaits: List<LigneFraisEnForfait>.from(json["LigneFraisEnForfaits"].map((x) => LigneFraisEnForfait.fromJson(x))),
        ligneFraisHorsForfaits: List<LigneFraisHorsForfait>.from(json["LigneFraisHorsForfaits"].map((x) => LigneFraisHorsForfait.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "mois": mois,
        "annee": annee,
        "dateModification": dateModification.toIso8601String(),
        "utilisateurId": utilisateurId,
        "LigneFraisEnForfaits": List<dynamic>.from(ligneFraisEnForfaits.map((x) => x.toJson())),
        "LigneFraisHorsForfaits": List<dynamic>.from(ligneFraisHorsForfaits.map((x) => x.toJson())),
    };
}
