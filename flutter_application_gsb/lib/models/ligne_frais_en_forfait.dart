import 'dart:convert';

List<LigneFraisEnForfait> ligneFraisEnForfaitFromJson(String str) => List<LigneFraisEnForfait>.from(json.decode(str).map((x) => LigneFraisEnForfait.fromJson(x)));

String ligneFraisEnForfaitToJson(List<LigneFraisEnForfait> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class LigneFraisEnForfait {
    LigneFraisEnForfait({
        required this.id,
        required this.quantite,
        required this.ficheFraisId,
        required this.fraisEnForfaitId,
    });

    int id;
    int quantite;
    int ficheFraisId;
    int fraisEnForfaitId;

    factory LigneFraisEnForfait.fromJson(Map<String, dynamic> json) => LigneFraisEnForfait(
        id: json["id"],
        quantite: json["quantite"],
        ficheFraisId: json["ficheFraisId"],
        fraisEnForfaitId: json["fraisEnForfaitId"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "quantite": quantite,
        "ficheFraisId": ficheFraisId,
        "fraisEnForfaitId": fraisEnForfaitId,
    };
}
