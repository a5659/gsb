// To parse this JSON data, do
//
//     final FraisEnForfait = FraisEnForfaitFromJson(jsonString);

import 'dart:convert';

List<FraisEnForfait> fraisEnForfaitFromJson(String str) =>
    List<FraisEnForfait>.from(
        json.decode(str).map((x) => FraisEnForfait.fromJson(x)));

String fraisEnForfaitToJson(List<FraisEnForfait> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FraisEnForfait {
  FraisEnForfait({
    required this.id,
    required this.libelle,
    required this.prixUnitaire,
  });

  int id;
  String libelle;
  String prixUnitaire;

  factory FraisEnForfait.fromJson(Map<String, dynamic> json) => FraisEnForfait(
        id: json["id"],
        libelle: json["libelle"],
        prixUnitaire: json["prixUnitaire"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "libelle": libelle,
        "prixUnitaire": prixUnitaire,
      };
}
