import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:modals/modals.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ModalTheme extends ModalEntry {
  final String tag;
  final int theme;
  final BuildContext context;

  ModalTheme(
      {Key? key, required this.theme, required this.tag, required this.context})
      : super.aligned(
          context,
          tag: tag,
          key: key,
          barrierDismissible: true,
          removeOnPop: true,
          alignment: Alignment.center,
          child: ToggleSwitch(
            minWidth: 90.0,
            minHeight: 70.0,
            initialLabelIndex: theme,
            cornerRadius: 20.0,
            activeFgColor: Colors.white,
            inactiveBgColor: Colors.grey,
            inactiveFgColor: Colors.white,
            totalSwitches: 2,
            icons: const [
              FontAwesomeIcons.sun,
              FontAwesomeIcons.moon,
            ],
            iconSize: 30.0,
            activeBgColors: const [
              [
                Color.fromARGB(115, 46, 91, 238),
                Color.fromARGB(213, 10, 235, 21)
              ],
              [Colors.yellow, Colors.orange]
            ],
            animate:
                true, // with just animate set to true, default curve = Curves.easeIn
            curve: Curves.bounceInOut,
            onToggle: (index) {
              if (index == 0) {
                AdaptiveTheme.of(context).setLight();
              } else {
                AdaptiveTheme.of(context).setDark();
              }
            },
          ),
        );
}
