import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LigneHorsForfait extends StatefulWidget {
  final void Function(int index) deleteAction;
  final void Function(int index, Map<String, dynamic> ligneData) updateAction;
  final int index;
  Map<String, dynamic> ligneData = {};

  LigneHorsForfait({
    Key? key,
    required this.deleteAction,
    required this.updateAction,
    required this.index,
    required this.ligneData,
  }) : super(key: key);

  @override
  State<LigneHorsForfait> createState() => _LigneHorsForfaitState();
}

class _LigneHorsForfaitState extends State<LigneHorsForfait> {
  get child => null;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final RegExp isNumericRegExp = RegExp(r'^\d+$');

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        // Select de Frais en Forfait
        SizedBox(
          width: 120.0,
          child: TextFormField(
            validator: (value) =>
                value!.isEmpty ? 'Veuillez entrer le libellé du frais' : null,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Libellé',
            ),
            onChanged: (value) {
              widget.ligneData['libelle'] = value;
              widget.updateAction(widget.index, widget.ligneData);
            },
          ),
        ),
        // Quantité
        SizedBox(
          width: 120.0,
          child: TextFormField(
            keyboardType: const TextInputType.numberWithOptions(decimal: true),
            inputFormatters: [
              FilteringTextInputFormatter.allow(RegExp('[0-9.]')),
            ],
            validator: (value) =>
                value!.isEmpty ? 'Veuillez entrer un prix' : null,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Prix unitaire',
            ),
            onChanged: (value) {
              widget.ligneData['prixUnitaire'] = double.parse(value);
              widget.updateAction(widget.index, widget.ligneData);
            },
          ),
        ),
        // Quantité
        SizedBox(
          width: 120.0,
          child: TextFormField(
            keyboardType: TextInputType.number,
            inputFormatters: [FilteringTextInputFormatter.digitsOnly],
            validator: (value) =>
                value!.isEmpty || !isNumericRegExp.hasMatch(value)
                    ? 'Veuillez entrer un nombre supérieur à 0'
                    : null,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Quantité',
            ),
            onChanged: (value) {
              widget.ligneData['quantite'] = int.parse(value);
              widget.updateAction(widget.index, widget.ligneData);
            },
          ),
        ),
        // Bouton suppression
        Container(
          height: 25,
          width: 25,
          decoration: BoxDecoration(
              color: Colors.red, borderRadius: BorderRadius.circular(20)),
          child: ElevatedButton(
            onPressed: () {
              widget.deleteAction(widget.index);
              //NouvelleFicheUtilisateur().method();
            },
            child: const Text(
              'X',
              style: TextStyle(fontSize: 25),
            ),
          ),
        ),
      ],
    );
  }
}
