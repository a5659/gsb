import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../models/frais_en_forfait.dart';
import '../../services/frais_en_forfait.dart';

class LigneEnForfait extends StatefulWidget {
  final void Function(int index) deleteAction;
  final void Function(int index, Map<String, int> ligneData) updateAction;
  final int index;

  late List<Map<String, Object>> listFrais = [];
  Map<String, int> ligneData = {};

  LigneEnForfait(
      {Key? key,
      required this.deleteAction,
      required this.updateAction,
      required this.index,
      required this.ligneData,
      required this.listFrais})
      : super(key: key);

  @override
  State<LigneEnForfait> createState() => _LigneEnForfaitState();
}

class _LigneEnForfaitState extends State<LigneEnForfait> {
  get child => null;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  late Future<List<FraisEnForfait>?> listFraisEnForfait;
  FraisEnForfait? frais;

  final RegExp isNumericRegExp = RegExp(r'^\d+$');

  @override
  initState() {
    super.initState();
    listFraisEnForfait = FraisEnForfaitService().fetchFraisEnForfait();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        // Select de Frais en Forfait
        SizedBox(
            width: 150.0,
            child: FutureBuilder<List<FraisEnForfait>?>(
              future: listFraisEnForfait,
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return DropdownButton(
                    hint: frais == null
                        ? const Text('Type de Frais')
                        : Text(frais!.libelle),
                    style: const TextStyle(color: Colors.blue),
                    items: snapshot.data.map<DropdownMenuItem<Object>>((item) {
                      return DropdownMenuItem<Object>(
                        value: item,
                        child: Text(item.libelle),
                      );
                    }).toList(),
                    onChanged: (value) {
                      //print(widget.ligneData);
                      setState(() {
                        frais = value as FraisEnForfait;
                        widget.ligneData['fraisEnForfaitId'] = frais!.id;
                        widget.updateAction(widget.index, widget.ligneData);
                      });
                    },
                  );
                } else if (snapshot.hasError) {
                  return Center(child: Text('${snapshot.error}'));
                }
                return const Center(
                  child: CircularProgressIndicator(
                      backgroundColor: Colors.cyanAccent),
                );
              },
            )),
        /*
        // Test de FormSelect
        Container(
          width: 250.0,
          child: SelectFormField(
            initialValue: widget.ligneData['fraisEnForfaitId'].toString(),
            labelText: "Type de frais",
            type: SelectFormFieldType.dropdown,
            items: [{"value":1, "label": "lala"}, {"value":2, "label":"lolo"}],//[widget.listFrais],
            /*
            validator: (value) => value!.isEmpty
                ? 'Veuillez choisir un type de frais'
                : null,
            */
            onChanged: (value) => setState(
                () => widget.ligneData['fraisEnForfaitId'] = int.parse(value)),
          ),
        ),
        */
        // Quantité
        Row(
          children: [
            SizedBox(
              width: 100.0,
              child: TextFormField(
                keyboardType: TextInputType.number,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                initialValue: widget.ligneData['quantite'].toString(),
                validator: (value) =>
                    value!.isEmpty || !isNumericRegExp.hasMatch(value)
                        ? 'Veuillez entrer un nombre supérieur à 0'
                        : null,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Quantité',
                ),
                onChanged: (value) {
                  widget.ligneData['quantite'] = int.parse(value);
                  widget.updateAction(widget.index, widget.ligneData);
                },
              ),
            ),
            // Bouton suppression
            InkWell(
                onTap: () {
                  widget.deleteAction(widget.index);
                  //NouvelleFicheUtilisateur().method();
                },
                child: const Icon(
                  Icons.cancel_outlined,
                  color: Colors.red,
                  size: 50,
                )),
          ],
        )
      ],
    );
  }
}
