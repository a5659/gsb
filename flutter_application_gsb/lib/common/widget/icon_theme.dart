import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';

import '../../screens/common/get_theme.dart';

class IconThemeApp extends StatelessWidget {
  const IconThemeApp({Key? key}) : super(key: key);
  final String theme = 'light';

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.brightness_6),
      tooltip: 'theme',
      onPressed: () async {
        await GetTheme().getCurrentTheme().then((value) {
          if (value['theme'] == theme) {
            AdaptiveTheme.of(context).setDark();
            // showModal(ModalTheme(
            //   tag: 'modalTheme',
            //   theme: 0,
            //   context: context,
            // ));
          } else {
            AdaptiveTheme.of(context).setLight();
            // showModal(ModalTheme(
            //   tag: 'modalTheme',
            //   theme: 1,
            //   context: context,
            // ));
          }
        });
      },
    );
  }
}
