import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_gsb/route_generator.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get_storage/get_storage.dart';

void main() async {
  // recuperation des variables d'environnement
  await dotenv.load();

  // initialisation du storage pour stockage du token
  await GetStorage.init();

  // lancement de l'application
  runApp(const MyApp());
}

Map<int, Color> color = {
  50: const Color.fromRGBO(39, 194, 230, .1),
  100: const Color.fromRGBO(39, 194, 230, .2),
  200: const Color.fromRGBO(39, 194, 230, .3),
  300: const Color.fromRGBO(39, 194, 230, .4),
  400: const Color.fromRGBO(39, 194, 230, .5),
  500: const Color.fromRGBO(252, 178, 111, .9),
  600: const Color.fromRGBO(39, 194, 230, .7),
  700: const Color.fromRGBO(39, 194, 230, .8),
  800: const Color.fromRGBO(39, 194, 230, .9),
  900: const Color.fromRGBO(39, 194, 230, 1),
};
MaterialColor colorCustom = MaterialColor(0xFF27C2E6, color);

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AdaptiveTheme(
      light: ThemeData(
        fontFamily: 'Poppins',
        brightness: Brightness.light,
        primarySwatch: colorCustom,
        cardColor: const Color.fromRGBO(39, 194, 230, .9),
      ),
      dark: ThemeData(
        fontFamily: 'Poppins',
        brightness: Brightness.dark,
        primarySwatch: Colors.orange,
      ),
      initial: AdaptiveThemeMode.light,
      builder: (theme, darkTheme) => MaterialApp(
        debugShowCheckedModeBanner: false,
        navigatorObservers: [RouteObserver<ModalRoute<dynamic>>()],
        title: 'Application GSB',
        theme: darkTheme,
        onGenerateRoute: RouteGenerator.generateRoute,
        initialRoute: '/login',
      ),
    );
  }
}
