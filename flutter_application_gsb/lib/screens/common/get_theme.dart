import 'package:adaptive_theme/adaptive_theme.dart';

class GetTheme {
  bool darkMode = false;
  dynamic savedThemeMode;
  var darkTheme = {
    "theme": "dark",
    "backgroundImage": "'assets/images/FondBlanc.png'",
  };
  var lightTheme = {
    "theme": "light",
    "backgroundImage": "'assets/images/FondNoir.jpg'",
  };
  Future getCurrentTheme() async {
    savedThemeMode = await AdaptiveTheme.getThemeMode();
    if (savedThemeMode.toString() == "AdaptiveThemeMode.dark") {
      return darkTheme;
    } else {
      return lightTheme;
    }
  }
}
