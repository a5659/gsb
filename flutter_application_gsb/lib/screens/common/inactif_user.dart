import 'package:flutter/material.dart';

import '../../common/widget/icon_theme.dart';

class InactifUser extends StatefulWidget {
  const InactifUser({Key? key}) : super(key: key);

  @override
  State<InactifUser> createState() => _InactifUserState();
}

class _InactifUserState extends State<InactifUser> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(actions: const <Widget>[
          IconThemeApp(),
        ]),
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Icon(
              Icons.warning,
              color: Colors.red,
              size: 50,
            ),
            Text(
              'Cet utilisateur est inactif',
              style: TextStyle(fontSize: 30),
              textAlign: TextAlign.center,
            )
          ],
        )));
  }
}
