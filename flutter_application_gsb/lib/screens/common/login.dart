import 'package:flutter/material.dart';
import 'package:flutter_application_gsb/services/auth.dart';
import 'package:get_storage/get_storage.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../common/widget/icon_theme.dart';
import 'forgot_pass.dart';
import '../../services/auth.dart';


class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);
  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  // final RegExp emailRegex = RegExp(
  //     r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");

  var identifiant = "", motDePasse = "", token = "";
  final storage = GetStorage();
  bool _isSecret = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(actions: const <Widget>[IconThemeApp()]),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60.0),
              child: Center(
                child: SizedBox(
                    width: 200,
                    height: 150,
                    /*decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0)),*/
                    child: Image.asset('assets/images/Logo-gsb.png')),
              ),
            ),
            Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextFormField(
                      onChanged: (value) => setState(() => identifiant = value),
                      // validator: (value) =>
                      //     value!.isEmpty || !emailRegex.hasMatch(value)
                      //         ? 'Please enter a valid email'
                      //         : null,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Identifiant',
                          hintText: 'Entrez votre identifiant'),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 15),
                      //padding: EdgeInsets.symmetric(horizontal: 15),
                      child: TextFormField(
                        obscureText: _isSecret,
                        onChanged: (value) =>
                            setState(() => motDePasse = value),
                        // validator: (value) => value!.length < 6
                        //     ? 'Please enter enter a password longer than 6 characters'
                        //     : null,
                        decoration: InputDecoration(
                            border: const OutlineInputBorder(),
                            labelText: 'Password',
                            suffixIcon: InkWell(
                              onTap: () =>
                                  setState(() => _isSecret = !_isSecret),
                              child: Icon(!_isSecret
                                  ? Icons.visibility
                                  : Icons.visibility_off),
                            ),
                            hintText: 'Enter secure password'),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 15.0, right: 15.0, top: 15, bottom: 15),
                      //padding: EdgeInsets.symmetric(horizontal: 15),
                      child: TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) => const ForgotPass()));
                        },
                        child: const Text('Forgot Password ?',
                            style: TextStyle(fontSize: 15)),
                      ),
                    ),
                    Container(
                      height: 50,
                      width: 250,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20)),
                      child: ElevatedButton(
                        onPressed: () async {
                          await Auth()
                              .login(identifiant, motDePasse)
                              .then((value) {
                            if (!value.data['success']) {
                              Fluttertoast.showToast(
                                  msg: value.data['message'],
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 10,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            } else {
                              token = value.data['token'];
                              // stockage du token dans le storage pour usage exterieur
                              storage.write('token', token);
                              Fluttertoast.showToast(
                                  msg: value.data['message'],
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 10,
                                  backgroundColor: Colors.green,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            }
                          });
                          var decoded = Auth().getInfo(token);
                          // if (decoded['situationID'] == 1) {
                          if (decoded['roleId'] == 3) {
                            Navigator.of(context).pushNamed('/admin');
                          } else if (decoded['roleId'] == 2) {
                            Navigator.of(context).pushNamed('/comptable');
                          } else if (decoded['roleId'] == 1) {
                            Navigator.of(context).pushNamed('/utilisateur');
                          }
                          // } else {
                          //   Navigator.of(context).pushNamed('/inactifUser');
                          // }
                        },
                        child: const Text(
                          'Connexion',
                          style: TextStyle(fontSize: 25),
                        ),
                      ),
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
