import 'package:flutter/material.dart';

import '../../common/widget/icon_theme.dart';

class HomePageComptable extends StatefulWidget {
  const HomePageComptable({Key? key}) : super(key: key);

  @override
  _HomePageComptableState createState() => _HomePageComptableState();
}

class _HomePageComptableState extends State<HomePageComptable> {
  get child => null;
  @override
  Widget build(BuildContext context) {
    return Container(
        constraints: const BoxConstraints.expand(),
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/FondBlanc.png"),
                fit: BoxFit.cover)),
        child: Scaffold(
          backgroundColor: Colors.transparent,

          //BARRE DE NAVIGATION
          appBar: AppBar(
              title: const Center(
                  child: Text(
                "Bienvenue !",
              )),
              leading: IconButton(
                icon: const Icon(Icons.logout_rounded),
                onPressed: () {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/login', (Route<dynamic> route) => false);
                },
              ),
              actions: const <Widget>[IconThemeApp()]),

          //CARTES SELECTION
          body: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                //Carte "fiches frais archivées"
                InkWell(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: const SizedBox(
                      height: 50,
                      child: Center(
                          child: Text(
                        'Fiches de frais archivées',
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/FicheArchiCompta');
                  },
                ),
                //Carte "fiches frais envoyées"
                InkWell(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: const SizedBox(
                      height: 50,
                      child: Center(
                          child: Text(
                        'Fiches de frais envoyées',
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/FicheEnvoyeCompta');
                  },
                ),
                //Carte "nouvelle fiche"
                InkWell(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: const SizedBox(
                      height: 50,
                      child: Center(
                          child: Text(
                        'Fiches validées pas encore remboursées',
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/ValidNonRembouCompta');
                  },
                )
              ]),
        ));
  }
}
