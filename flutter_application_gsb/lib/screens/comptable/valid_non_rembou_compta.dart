import 'package:flutter/material.dart';

import '../../common/widget/icon_theme.dart';

class ValidNonRembouComptable extends StatefulWidget {
  const ValidNonRembouComptable({Key? key}) : super(key: key);

  @override
  _ValidNonRembouComptableState createState() =>
      _ValidNonRembouComptableState();
}

class _ValidNonRembouComptableState extends State<ValidNonRembouComptable> {
  get child => null;
  @override
  Widget build(BuildContext context) {
    return Container(
        constraints: const BoxConstraints.expand(),
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/FondBlanc.png"),
                fit: BoxFit.cover)),
        child: Scaffold(
          backgroundColor: Colors.transparent,

          //BARRE DE NAVIGATION
          appBar: AppBar(
              title: const Center(
                  child: Text(
                "Fiches validées",
              )),
              leading: IconButton(
                icon: const Icon(Icons.logout_rounded),
                onPressed: () {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/login', (Route<dynamic> route) => false);
                },
              ),
              actions: const <Widget>[IconThemeApp()]),

          //CARTES SELECTION
          body: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                //Carte "Fiche n°1"
                InkWell(
                  child: Card(
                    color: const Color.fromARGB(200, 39, 194, 230),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: const SizedBox(
                      height: 50,
                      child: Center(
                          child: Text(
                        'Fiche n°1',
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/dashboardadmin');
                  },
                ),
                //Carte "Fiche n°2"
                InkWell(
                  child: Card(
                    color: const Color.fromARGB(200, 39, 194, 230),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: const SizedBox(
                      height: 50,
                      child: Center(
                          child: Text(
                        'Fiche n°2',
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/dashboardadmin');
                  },
                ),
                //Carte "Fiche n°3"
                InkWell(
                  child: Card(
                    color: const Color.fromARGB(200, 39, 194, 230),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: const SizedBox(
                      height: 50,
                      child: Center(
                          child: Text(
                        'Fiche n°3',
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/dashboardadmin');
                  },
                ),
              ]),
        ));
  }
}
