import 'package:flutter/material.dart';

import '../../common/widget/icon_theme.dart';

class HomePageUtilisateur extends StatefulWidget {
  const HomePageUtilisateur({Key? key}) : super(key: key);

  @override
  _HomePageUtilisateurState createState() => _HomePageUtilisateurState();
}

class _HomePageUtilisateurState extends State<HomePageUtilisateur> {
  get child => null;

  @override
  Widget build(BuildContext context) {
    return Container(
        constraints: const BoxConstraints.expand(),
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/FondBlanc.png"),
                fit: BoxFit.cover)),
        child: Scaffold(
          backgroundColor: Colors.transparent,

          //BARRE DE NAVIGATION
          appBar: AppBar(
              title: const Center(
                  child: Text(
                "Bienvenue !",
              )),
              leading: IconButton(
                icon: const Icon(Icons.logout_rounded),
                onPressed: () {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/login', (Route<dynamic> route) => false);
                },
              ),
              actions: const <Widget>[IconThemeApp()]),

          //CARTES SELECTION
          body: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                //Carte "mes infos"
                InkWell(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: const SizedBox(
                      height: 50,
                      child: Center(
                          child: Text(
                        'Mes infos',
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/MesInfosUtil');
                  },
                ),
                //Carte "fiches frais archivées"
                InkWell(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: const SizedBox(
                      height: 50,
                      child: Center(
                          child: Text(
                        'Fiches de frais archivées',
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/FicheArchiUtil');
                  },
                ),
                //Carte "fiches frais envoyées"
                InkWell(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: const SizedBox(
                      height: 50,
                      child: Center(
                          child: Text(
                        'Fiches de frais envoyées',
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/FicheEnvoyeUtil');
                  },
                ),
                //Carte "brouilons"
                InkWell(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: const SizedBox(
                      height: 50,
                      child: Center(
                          child: Text(
                        'Brouillons',
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/FicheBrouillonUtil');
                  },
                ),
                //Carte "nouvelle fiche"
                InkWell(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: const SizedBox(
                      height: 50,
                      child: Center(
                          child: Text(
                        'Nouvelle fiche',
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/NouvelleFicheUtil');
                  },
                )
              ]),
        ));
  }
}
