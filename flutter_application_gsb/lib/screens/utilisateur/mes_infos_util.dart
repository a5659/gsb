import 'package:flutter/material.dart';

import '../../common/widget/icon_theme.dart';

class MesInfosUtilisateur extends StatefulWidget {
  const MesInfosUtilisateur({Key? key}) : super(key: key);

  @override
  _MesInfosUtilisateurState createState() => _MesInfosUtilisateurState();
}

class _MesInfosUtilisateurState extends State<MesInfosUtilisateur> {
  get child => null;
  @override
  Widget build(BuildContext context) {
    return Container(
        constraints: const BoxConstraints.expand(),
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/FondBlanc.png"),
                fit: BoxFit.cover)),
        child: Scaffold(
          backgroundColor: Colors.transparent,

          //BARRE DE NAVIGATION
          appBar: AppBar(
              title: const Center(
                  child: Text(
                "Mes infos",
              )),
              leading: IconButton(
                icon: const Icon(Icons.logout_rounded),
                onPressed: () {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/login', (Route<dynamic> route) => false);
                },
              ),
              actions: const <Widget>[IconThemeApp()]),

          //CARTES SELECTION
          body: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                //Carte "mes infos p1"
                InkWell(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: const SizedBox(
                      height: 50,
                      child: Center(
                          child: Text(
                        'Nom Prénom?',
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  onTap: () {},
                ),
                //Carte "mes infos p2"
                InkWell(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: const SizedBox(
                      height: 50,
                      child: Center(
                          child: Text(
                        'Date naissance, embauche, autre ?',
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  onTap: () {},
                ),
                //Carte "mes infos p2"
                InkWell(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: const SizedBox(
                      height: 50,
                      child: Center(
                          child: Text(
                        'RIB ou autre moyen remboursement?',
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  onTap: () {},
                ),
              ]),
        ));
  }
}
