import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';

import '../../common/widget/icon_theme.dart';
import '../../services/auth.dart';
import '../../services/fiche.dart';
import '../../models/fiche.dart';

class FicheEnvoyeUtilisateur extends StatefulWidget {
  const FicheEnvoyeUtilisateur({Key? key}) : super(key: key);

  @override
  _FicheEnvoyeUtilisateurState createState() => _FicheEnvoyeUtilisateurState();
}

class _FicheEnvoyeUtilisateurState extends State<FicheEnvoyeUtilisateur> {
  get child => null;
  final storage = GetStorage();
  late Future<List<Fiche>?> listFiches;
  var separator = false;
  late String token;

  @override
  void initState() {
    super.initState();
    token = storage.read('token');
    var decodedToken = Auth().getInfo(storage.read('token'));

    listFiches =
        FicheService().fetchFichesByVisitor(decodedToken['utilisateurId']);
  }

  @override
  Widget build(BuildContext context) {
    //recuperation du token dans le storage, decodage et lecture de l'id d'utilisateur

    return Scaffold(
        backgroundColor: Colors.transparent,

        //BARRE DE NAVIGATION
        appBar: AppBar(
            title: const Center(
                child: Text(
              "Fiches envoyées",
            )),
            leading: IconButton(
              icon: const Icon(Icons.logout_rounded),
              onPressed: () {
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/login', (Route<dynamic> route) => false);
              },
            ),
            actions: const <Widget>[IconThemeApp()]),

        //CARTES SELECTION
        body: Stack(children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/FondNoir.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          FutureBuilder<List<Fiche>?>(
              future: listFiches,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView.separated(
                      padding: const EdgeInsets.only(bottom: 100),
                      itemBuilder: (context, index) {
                        var fiche = (snapshot.data as List<Fiche>)[index];
                        separator = false;
                        if (fiche.etat == 3) {
                          return InkWell(
                            child: Card(
                              color: const Color.fromARGB(200, 39, 194, 230),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              child: SizedBox(
                                height: 50,
                                child: Center(
                                    child: Text(
                                  "Fiche du ${fiche.mois} / ${fiche.annee}",
                                  textAlign: TextAlign.center,
                                  overflow: TextOverflow.ellipsis,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold),
                                )),
                              ),
                            ),
                            onTap: () {
                              Navigator.of(context)
                                  .pushNamed('/UpdateFicheUtil');
                            },
                          );
                        } else {
                          return const SizedBox(width: 0.0, height: 0.0);
                        }
                      },
                      separatorBuilder: (context, index) {
                        return Divider(
                          color: Colors.transparent,
                          height: separator == true ? 0 : 10,
                        );
                      },
                      itemCount: (snapshot.data as List<Fiche>).length);
                } else if (snapshot.hasError) {
                  return Center(child: Text('${snapshot.error}'));
                }
                return const Center(
                  child: CircularProgressIndicator(
                      backgroundColor: Colors.cyanAccent),
                );
              })
        ]));
  }
}
