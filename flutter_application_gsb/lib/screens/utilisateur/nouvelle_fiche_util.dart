import 'package:flutter/material.dart';
import 'package:flutter_application_gsb/common/widget/icon_theme.dart';
import 'package:get_storage/get_storage.dart';

import '../../services/auth.dart';
import '../../services/fiche.dart';
import '../../services/ligne_en_forfait.dart';
import '../../services/ligne_hors_forfait.dart';
import '../../services/frais_en_forfait.dart';

import '../../models/frais_en_forfait.dart';

import '../../common/widget/ligne_frais_en_forfait.dart';
import '../../common/widget/ligne_frais_hors_forfait.dart';

class NouvelleFicheUtilisateur extends StatefulWidget {
  const NouvelleFicheUtilisateur({Key? key}) : super(key: key);

  @override
  _NouvelleFicheUtilisateurState createState() =>
      _NouvelleFicheUtilisateurState();
}

class _NouvelleFicheUtilisateurState extends State<NouvelleFicheUtilisateur> {
  get child => null;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  var decodedToken = Auth().getInfo(GetStorage().read('token'));

  var data = {
    "mois": "",
    "annee": "",
    "utilisateuId": -1,
    "etat": 4,
    "ligneFraisEnForfaits": [],
    "ligneFraisHorsForfaits": [],
  };
  late Future<List<FraisEnForfait>?> listFraisEnForfait;

  final RegExp monthRegexp = RegExp(r'^(0?[1-9]|1[012])$');
  final RegExp yearRegexp = RegExp(r'^(19|20)\d{2}$');

  @override
  void initState() {
    super.initState();
    data["utilisateurId"] = decodedToken["utilisateurId"];
    listFraisEnForfait = FraisEnForfaitService().fetchFraisEnForfait();
  }

  _deleteLigneEnForfait(int index) {
    (data['ligneFraisEnForfaits'] as List).removeAt(index);
    setState(() {});
  }

  _deleteLigneHorsForfait(int index) {
    (data['ligneFraisHorsForfaits'] as List).removeAt(index);
    setState(() {});
  }

  _updateLigneEnForfait(int index, Map<String, int> ligneData) {
    (data['ligneFraisEnForfaits'] as List)[index] = ligneData;
  }

  _updateLigneHorsForfait(int index, Map<String, dynamic> ligneData) {
    (data['ligneFraisHorsForfaits'] as List)[index] = ligneData;
  }

  creerBrouillon() async {
    await FicheService().createFiche(data).then((ficheResponse) async {
      var newFicheId = ficheResponse.data['id'];
      await Future.wait(
          (data['ligneFraisEnForfaits'] as List).map((item) async {
        item['ficheFraisId'] = newFicheId;
        await LigneEnForfaitService().createLigneEnForfait(item);
      }));
      await Future.wait(
          (data['ligneFraisHorsForfaits'] as List).map((item) async {
        item['ficheFraisId'] = newFicheId;
        await LigneHorsForfaitService().createLigneHorsForfait(item);
      }));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

        //BARRE DE NAVIGATION
        appBar: AppBar(
            title: const Center(
                child: Text(
              "",
            )),
            leading: IconButton(
              icon: const Icon(Icons.logout_rounded),
              onPressed: () {
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/login', (Route<dynamic> route) => false);
              },
            ),
            actions: const <Widget>[IconThemeApp()]),

        //FORMULAIRE
        body: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
            child: Column(children: <Widget>[
              const Text(
                'Ajouter une nouvelle fiche',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
              ),
              Form(
                  key: _formKey,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(height: 20),
                        TextFormField(
                          onChanged: (value) =>
                              setState(() => data["mois"] = value),
                          validator: (value) =>
                              value!.isEmpty || !monthRegexp.hasMatch(value)
                                  ? 'Veuillez entrer un nombre entre 1 et 12'
                                  : null,
                          decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Mois',
                              hintText: "1 à 12"),
                        ),
                        const SizedBox(height: 20),
                        TextFormField(
                          onChanged: (value) =>
                              setState(() => data["annee"] = value),
                          validator: (value) =>
                              value!.isEmpty || !yearRegexp.hasMatch(value)
                                  ? 'Veuillez entrer une année valide'
                                  : null,
                          decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Année',
                              hintText: "format XXXX"),
                        ),
                        const SizedBox(height: 20),
                        // Partie Lignes frais en forfait
                        const Text(
                          'Frais forfaitisés',
                          style: TextStyle(fontSize: 20),
                        ),
                        const SizedBox(height: 20),

                        SizedBox(
                          height: 90.0 *
                              (data['ligneFraisEnForfaits'] as List).length,
                          child: ListView.builder(
                              padding: const EdgeInsets.all(8),
                              itemCount:
                                  (data['ligneFraisEnForfaits'] as List).length,
                              itemBuilder: (BuildContext context, int index) {
                                return SizedBox(
                                  height: 80,
                                  child: LigneEnForfait(
                                    updateAction: _updateLigneEnForfait,
                                    deleteAction: _deleteLigneEnForfait,
                                    index: index,
                                    ligneData: const {
                                      'fraisEnForfaitId': 1,
                                      'quantite': 0
                                    },
                                    listFrais: const [
                                      {'value': 1, 'libelle': "Test"}
                                    ],
                                  ),
                                );
                              }),
                        ),
                        // bouton ajout Ligne en forfait
                        Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20)),
                          child: ElevatedButton(
                            onPressed: () {
                              (data['ligneFraisEnForfaits'] as List).add("");
                              setState(() {}); // lance le redraw de l'ecran
                            },
                            child: const Text(
                              '+',
                              style: TextStyle(fontSize: 25),
                            ),
                          ),
                        ),

                        // Partie Lignes hors forfait
                        const Text(
                          'Frais hors forfait',
                          style: TextStyle(fontSize: 20),
                        ),
                        const SizedBox(height: 20),
                        SizedBox(
                          height: 50.0 *
                              (data['ligneFraisHorsForfaits'] as List).length,
                          child: ListView.builder(
                              padding: const EdgeInsets.all(8),
                              itemCount:
                                  (data['ligneFraisHorsForfaits'] as List)
                                      .length,
                              itemBuilder: (BuildContext context, int index) {
                                return SizedBox(
                                  height: 50,
                                  child: LigneHorsForfait(
                                    updateAction: _updateLigneHorsForfait,
                                    deleteAction: _deleteLigneHorsForfait,
                                    index: index,
                                    ligneData: const {'quantite': 0},
                                  ),
                                );
                              }),
                        ),
                        const SizedBox(height: 20),
                        // bouton ajout Ligne hors forfait
                        Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20)),
                          child: ElevatedButton(
                            onPressed: () {
                              (data['ligneFraisHorsForfaits'] as List)
                                  .add("lala");
                              setState(() {}); // lance le redraw de l'ecran
                            },
                            child: const Text(
                              '+',
                              style: TextStyle(fontSize: 25),
                            ),
                          ),
                        ),

                        // Bouton validation de création
                        const SizedBox(height: 20),
                        Container(
                          height: 50,
                          width: 250,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20)),
                          child: ElevatedButton(
                            onPressed: () {
                              creerBrouillon();
                              Navigator.of(context).pushNamed('/utilisateur');
                            },
                            child: const Text(
                              'Ajouter',
                              style: TextStyle(fontSize: 25),
                            ),
                          ),
                        ),
                      ]))
            ])));
  }
}
