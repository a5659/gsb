import 'package:flutter/material.dart';
import 'package:flutter_application_gsb/common/widget/icon_theme.dart';
import 'package:flutter_application_gsb/models/ligne_frais_en_forfait.dart';
import 'package:get_storage/get_storage.dart';

import '../../services/auth.dart';
import '../../services/fiche.dart';

import '../../models/fiche.dart';
import '../../models/ligne_frais_en_forfait.dart';
import '../../models/ligne_frais_hors_forfait.dart';

import '../../common/widget/ligne_frais_en_forfait.dart';
import '../../common/widget/ligne_frais_hors_forfait.dart';

class UpdateFiche extends StatefulWidget {
  const UpdateFiche({Key? key, required this.id}) : super(key: key);
  final int id;

  @override
  _UpdateFicheState createState() => _UpdateFicheState();
}

class _UpdateFicheState extends State<UpdateFiche> {
  get child => null;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  var decodedToken = Auth().getInfo(GetStorage().read('token'));

  late Future<Fiche?> fiche;

/*
  var data = {
    "mois": fiche.mois,
    "annee": "",
    "utilisateuId": -1,
    "etat": 4,
    "ligneFraisEnForfaits": [],
    "ligneFraisHorsForfaits": [],
  };
  */

  var deletedLignesEnForfait = [];
  var deletedLignesHorsForfait = [];

  final RegExp monthRegexp = RegExp(r'^(0?[1-9]|1[012])$');
  final RegExp yearRegexp = RegExp(r'^(19|20)\d{2}$');

  @override
  void initState() {
    super.initState();
    fiche = FicheService().fetchOneFiche(widget.id);
  }

  _deleteLigneEnForfait(int index) {
    LigneFraisEnForfait deleted =
        (fiche as Fiche).ligneFraisEnForfaits.removeAt(index);
    deletedLignesEnForfait.add(deleted.id);
    setState(() {});
  }

  _deleteLigneHorsForfait(int index) {
    LigneFraisHorsForfait deleted =
        (fiche as Fiche).ligneFraisHorsForfaits.removeAt(index);
    deletedLignesHorsForfait.add(deleted.id);
    setState(() {});
  }

  _updateLigneEnForfait(int index, Map<String, int> ligneData) {
    (fiche as Fiche).ligneFraisEnForfaits[index] =
        ligneData as LigneFraisEnForfait;
  }

  _updateLigneHorsForfait(int index, Map<String, dynamic> ligneData) {
    (fiche as Fiche).ligneFraisHorsForfaits[index] =
        ligneData as LigneFraisHorsForfait;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        constraints: const BoxConstraints.expand(),
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/FondBlanc.png"),
                fit: BoxFit.cover)),
        child: Scaffold(
            backgroundColor: Colors.transparent,

            //BARRE DE NAVIGATION
            appBar: AppBar(
                title: const Center(
                    child: Text(
                  "",
                )),
                leading: IconButton(
                  icon: const Icon(Icons.logout_rounded),
                  onPressed: () {
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        '/login', (Route<dynamic> route) => false);
                  },
                ),
                actions: const <Widget>[IconThemeApp()]),

            //FORMULAIRE
            body: SingleChildScrollView(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                child: Column(children: <Widget>[
                  const Text(
                    'Modifier une fiche',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                  ),
                  FutureBuilder<Fiche?>(
                    future: fiche,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        var fiche = (snapshot.data as Fiche);
                        var data = {
                          "mois": fiche.mois,
                          "annee": fiche.annee,
                          "ligneFraisEnForfaits": fiche.ligneFraisEnForfaits,
                          "ligneFraisHorsForfaits":
                              fiche.ligneFraisHorsForfaits,
                        };
                        return Form(
                            key: _formKey,
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const SizedBox(height: 20),
                                  TextFormField(
                                    initialValue: fiche.mois,
                                    onChanged: (value) =>
                                        setState(() => data["mois"] = value),
                                    validator: (value) => value!.isEmpty ||
                                            !monthRegexp.hasMatch(value)
                                        ? 'Veuillez entrer un nombre entre 1 et 12'
                                        : null,
                                    decoration: const InputDecoration(
                                        border: OutlineInputBorder(),
                                        labelText: 'Mois',
                                        hintText: "1 à 12"),
                                  ),
                                  const SizedBox(height: 20),
                                  TextFormField(
                                    initialValue: fiche.annee,
                                    onChanged: (value) =>
                                        setState(() => data["annee"] = value),
                                    validator: (value) => value!.isEmpty ||
                                            !yearRegexp.hasMatch(value)
                                        ? 'Veuillez entrer une année valide'
                                        : null,
                                    decoration: const InputDecoration(
                                        border: OutlineInputBorder(),
                                        labelText: 'Année',
                                        hintText: "format XXXX"),
                                  ),
                                  const SizedBox(height: 20),
                                  // Partie Lignes frais en forfait
                                  const Text(
                                    'Frais forfaitisés',
                                    style: TextStyle(fontSize: 20),
                                  ),
                                  const SizedBox(height: 20),

                                  SizedBox(
                                    height: 90.0 *
                                        (data['ligneFraisEnForfaits'] as List)
                                            .length,
                                    child: ListView.builder(
                                        padding: const EdgeInsets.all(8),
                                        itemCount: (data['ligneFraisEnForfaits']
                                                as List)
                                            .length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return SizedBox(
                                            height: 80,
                                            child: LigneEnForfait(
                                              updateAction:
                                                  _updateLigneEnForfait,
                                              deleteAction:
                                                  _deleteLigneEnForfait,
                                              index: index,
                                              ligneData: {
                                                'fraisEnForfaitId': fiche
                                                    .ligneFraisEnForfaits[index]
                                                    .id,
                                                'quantite': fiche
                                                    .ligneFraisEnForfaits[index]
                                                    .quantite
                                              },
                                              listFrais: [],
                                            ),
                                          );
                                        }),
                                  ),
                                  // bouton ajout Ligne en forfait
                                  Container(
                                    height: 50,
                                    width: 50,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: ElevatedButton(
                                      onPressed: () {
                                        (data['ligneFraisEnForfaits'] as List)
                                            .add("");
                                        setState(
                                            () {}); // lance le redraw de l'ecran
                                      },
                                      child: const Text(
                                        '+',
                                        style: TextStyle(fontSize: 25),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 35,
                                  ),
                                  // Partie Lignes hors forfait
                                  const Text(
                                    'Frais hors forfait',
                                    style: TextStyle(fontSize: 20),
                                  ),
                                  const SizedBox(height: 20),
                                  SizedBox(
                                    height: 50.0 *
                                        (data['ligneFraisHorsForfaits'] as List)
                                            .length,
                                    child: ListView.builder(
                                        padding: const EdgeInsets.all(8),
                                        itemCount:
                                            (data['ligneFraisHorsForfaits']
                                                    as List)
                                                .length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return SizedBox(
                                            height: 50,
                                            child: LigneHorsForfait(
                                              updateAction:
                                                  _updateLigneHorsForfait,
                                              deleteAction:
                                                  _deleteLigneHorsForfait,
                                              index: index,
                                              ligneData: const {'quantite': 0},
                                            ),
                                          );
                                        }),
                                  ),
                                  const SizedBox(height: 20),
                                  // bouton ajout Ligne hors forfait
                                  Container(
                                    height: 50,
                                    width: 50,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: ElevatedButton(
                                      onPressed: () {
                                        (data['ligneFraisHorsForfaits'] as List)
                                            .add("lala");
                                        setState(
                                            () {}); // lance le redraw de l'ecran
                                      },
                                      child: const Text(
                                        '+',
                                        style: TextStyle(fontSize: 25),
                                      ),
                                    ),
                                  ),

                                  // Bouton validation de création
                                  const SizedBox(height: 20),
                                  Container(
                                    height: 50,
                                    width: 250,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: ElevatedButton(
                                      onPressed: () {
                                        //creerBrouillon();
                                      },
                                      child: const Text(
                                        'Confirmer',
                                        style: TextStyle(fontSize: 25),
                                      ),
                                    ),
                                  ),
                                ]));
                      } else if (snapshot.hasError) {
                        return Center(child: Text('${snapshot.error}'));
                      }
                      return const Center(
                        child: CircularProgressIndicator(
                            backgroundColor: Colors.cyanAccent),
                      );
                    },
                  )
                ]))));
  }
}
