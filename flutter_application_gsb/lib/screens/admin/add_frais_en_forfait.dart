import 'package:flutter/material.dart';
import 'package:flutter_application_gsb/common/widget/icon_theme.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../services/frais_en_forfait.dart';

class AddFraisEnForfait extends StatefulWidget {
  const AddFraisEnForfait({Key? key}) : super(key: key);

  @override
  State<AddFraisEnForfait> createState() => _AddFraisEnForfaitState();
}

class _AddFraisEnForfaitState extends State<AddFraisEnForfait> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  var data = {
    "libelle": "",
    "prixUnitaire": "",
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(actions: const <Widget>[IconThemeApp()]),
        body: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
            child: Column(children: <Widget>[
              const Text(
                'Ajouter un nouveau frais en forfait',
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
              ),
              const SizedBox(height: 20),
              Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(height: 20),
                      TextFormField(
                        onChanged: (value) =>
                            setState(() => data["libelle"] = value),
                        validator: (value) =>
                            value!.isEmpty ? 'Ce champ est requis' : null,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Libelle*',
                            hintText: "Entrez le libelle du frais en forfait"),
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        onChanged: (value) =>
                            setState(() => data["prixUnitaire"] = value),
                        validator: (value) =>
                            value!.isEmpty ? 'Ce champ est requis' : null,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Prix Unitaire*',
                            hintText:
                                "Entrez le prix unitaire du frais en forfait"),
                      ),
                      const SizedBox(height: 20),
                      Container(
                        height: 50,
                        width: 250,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20)),
                        child: ElevatedButton(
                          onPressed: () async {
                            if (_formKey.currentState!.validate()) {
                              await FraisEnForfaitService()
                                  .addFraisEnForfait(data)
                                  .then((value) {
                                if (value.statusMessage == 'OK') {
                                  Fluttertoast.showToast(
                                      msg: 'Frais en forfait créé avec succès',
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 10,
                                      backgroundColor: Colors.green,
                                      textColor: Colors.white,
                                      fontSize: 16.0);
                                  Navigator.of(context)
                                      .pushNamed('/adminfraisforfait');
                                } else {
                                  Fluttertoast.showToast(
                                      msg: value.data['message'],
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 10,
                                      backgroundColor: Colors.red,
                                      textColor: Colors.white,
                                      fontSize: 16.0);
                                }
                              });
                            }
                          },
                          child: const Text(
                            'Ajouter',
                            style: TextStyle(fontSize: 25),
                          ),
                        ),
                      ),
                    ],
                  ))
            ])));
  }
}
