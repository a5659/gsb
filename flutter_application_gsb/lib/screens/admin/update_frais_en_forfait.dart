import 'package:flutter/material.dart';
import 'package:flutter_application_gsb/common/widget/icon_theme.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../models/frais_en_forfait.dart';
import '../../services/frais_en_forfait.dart';

class UpdateFraisEnForfait extends StatefulWidget {
  const UpdateFraisEnForfait({Key? key, required this.id}) : super(key: key);
  final int id;

  @override
  State<UpdateFraisEnForfait> createState() => _UpdateFraisEnForfaitState();
}

class _UpdateFraisEnForfaitState extends State<UpdateFraisEnForfait> {
  late Future<FraisEnForfait?> frais;

  @override
  void initState() {
    super.initState();
    frais = FraisEnForfaitService().fetchOneFraisEnForfait(widget.id);
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(actions: const <Widget>[IconThemeApp()]),


        body: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
            child: Column(children: <Widget>[
              const Text(
                'Modifier un frais en forfait',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
              ),
              FutureBuilder<FraisEnForfait?>(
                future: frais,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    var frais = (snapshot.data as FraisEnForfait);
                    var data = {
                      "libelle": frais.libelle,
                      "prixUnitaire": frais.prixUnitaire,
                    };
                    return Form(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const SizedBox(height: 20),
                            TextFormField(
                              initialValue: frais.libelle,
                              onChanged: (value) =>
                                  setState(() => frais.libelle = value),
                              validator: (value) =>
                                  value!.isEmpty ? 'Ce champ est requis' : null,
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Libelle*',
                                  hintText:
                                      "Entrez le libelle du frais en forfait"),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                                initialValue: frais.prixUnitaire.toString(),
                                onChanged: (value) => setState(() => frais
                                    .prixUnitaire = value.replaceAll(",", ".")),
                                validator: (value) => value!.isEmpty
                                    ? 'Ce champ est requis'
                                    : null,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  labelText: "Prix Unitaire*",
                                  hintText:
                                      "Entrez le prix unitaire du frais en forfait",
                                )),
                            const SizedBox(height: 20),
                            Container(
                              height: 50,
                              width: 250,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20)),
                              child: ElevatedButton(
                                onPressed: () async {
                                  if (_formKey.currentState!.validate()) {
                                    await FraisEnForfaitService()
                                        .updateFraisEnForfait(data, frais.id)
                                        .then((value) {
                                      if (value.statusMessage == 'OK') {
                                        Fluttertoast.showToast(
                                            msg:
                                                'Utilisateur modifié avec succès',
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIosWeb: 10,
                                            backgroundColor: Colors.green,
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                        Navigator.of(context)
                                            .pushNamed('/adminfraisforfait');
                                      } else {
                                        Fluttertoast.showToast(
                                            msg: value.data['message'],
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIosWeb: 10,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                      }
                                    });
                                  }
                                },
                                child: const Text(
                                  'Modifier',
                                  style: TextStyle(fontSize: 25),
                                ),
                              ),
                            ),
                          ],
                        ));
                  } else if (snapshot.hasError) {
                    return Center(child: Text('${snapshot.error}'));
                  }
                  return const Center(
                    child: CircularProgressIndicator(
                        backgroundColor: Colors.cyanAccent),
                  );
                },
              )
            ])));
  }
}
