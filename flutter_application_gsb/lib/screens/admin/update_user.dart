import 'package:flutter/material.dart';
import 'package:flutter_application_gsb/common/widget/icon_theme.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:select_form_field/select_form_field.dart';

import '../../models/utilisateur.dart';
import '../../services/utilisateur.dart';

class UpdateUser extends StatefulWidget {
  const UpdateUser({Key? key, required this.id}) : super(key: key);
  final int id;

  @override
  State<UpdateUser> createState() => _UpdateUserState();
}

class _UpdateUserState extends State<UpdateUser> {
  late Future<Utilisateur?> user;

  @override
  void initState() {
    super.initState();
    user = UtilisateurService().fetchOneUsers(widget.id);
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final RegExp emailRegex = RegExp(
      r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
  final RegExp passwordRegex =
      RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$');

  final List<Map<String, dynamic>> _items = [
    {'value': 1, 'label': 'Visiteur'},
    {'value': 2, 'label': 'Comptable'},
    {'value': 3, 'label': 'Admin'},
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(actions: const <Widget>[IconThemeApp()]),
        body: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
            child: Column(children: <Widget>[
              const Text(
                'Modifier un utilisateur',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
              ),
              FutureBuilder<Utilisateur?>(
                future: user,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    var user = (snapshot.data as Utilisateur);
                    var data = {
                      "nom": user.nom,
                      "prenom": user.prenom,
                      "email": user.email,
                      "identifiant": user.identifiant,
                      "motDePasse": user.motDePasse,
                      "dateNaissance": user.dateNaissance.toIso8601String(),
                      "dateEmbauche": user.dateEmbauche.toIso8601String(),
                      "adresse": user.adresse,
                      "codePostal": user.codePostal,
                      "ville": user.ville,
                      "roleId": user.roleId.toString()
                    };
                    return Form(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const SizedBox(height: 20),
                            TextFormField(
                              initialValue: user.nom,
                              onChanged: (value) =>
                                  setState(() => user.nom = value),
                              validator: (value) =>
                                  value!.isEmpty ? 'Ce champ est requis' : null,
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Nom*',
                                  hintText: "Entrez le nom de l'utilisateur"),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              initialValue: user.prenom,
                              onChanged: (value) =>
                                  setState(() => user.prenom = value),
                              validator: (value) =>
                                  value!.isEmpty ? 'Ce champ est requis' : null,
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Prénom*',
                                  hintText:
                                      "Entrez le prénom de l'utilisateur"),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              initialValue: user.email,
                              onChanged: (value) =>
                                  setState(() => user.email = value),
                              validator: (value) =>
                                  value!.isEmpty || !emailRegex.hasMatch(value)
                                      ? 'Please enter a valid email'
                                      : null,
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Email*',
                                  hintText: "Entrez l'email de l'utilisateur"),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              initialValue: user.identifiant,
                              onChanged: (value) =>
                                  setState(() => user.identifiant = value),
                              validator: (value) =>
                                  value!.isEmpty ? 'Ce champ est requis' : null,
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Identifiant*',
                                  hintText:
                                      "Entrez l'identifiant de l'utilisateur"),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              initialValue: user.motDePasse,
                              onChanged: (value) =>
                                  setState(() => user.motDePasse = value),
                              validator: (value) => value!.isEmpty ||
                                      !passwordRegex.hasMatch(value)
                                  ? 'Votre mot de passe doit contenir au moins 8 caractères dont une majuscule, une minuscule, un chiffre et un des caractères spéciaux suivants: ! @ # \$ & * ~ '
                                  : null,
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  errorMaxLines: 4,
                                  labelText: 'Mot de passe*',
                                  hintText:
                                      "Entrez le mot de passe de l'utilisateur"),
                            ),
                            const SizedBox(height: 20),
                            DateTimePicker(
                              initialValue:
                                  user.dateNaissance.toIso8601String(),
                              firstDate: DateTime(2000),
                              lastDate: DateTime(2100),
                              dateLabelText:
                                  "Date de naissance de l'utilisateur",
                              onChanged: (value) => setState(
                                  () => user.dateNaissance = value as DateTime),
                            ),
                            const SizedBox(height: 20),
                            DateTimePicker(
                              initialValue: user.dateEmbauche.toIso8601String(),
                              firstDate: DateTime(2000),
                              lastDate: DateTime(2100),
                              dateLabelText:
                                  "Date d'embauche de l'utilisateur*",
                              validator: (value) =>
                                  value!.isEmpty ? 'Ce champ est requis' : null,
                              onChanged: (value) => setState(
                                  () => user.dateEmbauche = value as DateTime),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              initialValue: user.adresse,
                              onChanged: (value) =>
                                  setState(() => user.adresse = value),
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Adresse',
                                  hintText:
                                      "Entrez l'adresse de l'utilisateur"),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              initialValue: user.codePostal,
                              onChanged: (value) =>
                                  setState(() => user.codePostal = value),
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Code postal',
                                  hintText:
                                      "Entrez le code postal de l'utilisateur"),
                              maxLength: 5,
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              initialValue: user.ville,
                              onChanged: (value) =>
                                  setState(() => user.ville = value),
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Ville',
                                  hintText: "Entrez la ville de l'utilisateur"),
                            ),
                            const SizedBox(height: 20),
                            SelectFormField(
                              initialValue: user.roleId.toString(),
                              labelText: "Role de l'utilisateur",
                              type: SelectFormFieldType.dropdown,
                              items: _items,
                              validator: (value) => value!.isEmpty
                                  ? 'Veuillez choisir un rôle'
                                  : null,
                              onChanged: (value) => setState(
                                  () => user.roleId = int.parse(value)),
                            ),
                            const SizedBox(height: 20),
                            Container(
                              height: 50,
                              width: 250,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20)),
                              child: ElevatedButton(
                                onPressed: () async {
                                  if (_formKey.currentState!.validate()) {
                                    await UtilisateurService()
                                        .updateUser(data, user.id)
                                        .then((value) {
                                      if (value.statusMessage == 'OK') {
                                        Fluttertoast.showToast(
                                            msg:
                                                'Utilisateur modifié avec succès',
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIosWeb: 10,
                                            backgroundColor: Colors.green,
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                        Navigator.of(context)
                                            .pushNamed('/dashboardadmin');
                                      } else {
                                        Fluttertoast.showToast(
                                            msg: value.data['message'],
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIosWeb: 10,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                      }
                                    });
                                  }
                                },
                                child: const Text(
                                  'Modifier',
                                  style: TextStyle(fontSize: 25),
                                ),
                              ),
                            ),
                          ],
                        ));
                  } else if (snapshot.hasError) {
                    return Center(child: Text('${snapshot.error}'));
                  }
                  return const Center(
                    child: CircularProgressIndicator(
                        backgroundColor: Colors.cyanAccent),
                  );
                },
              )
            ])));
  }
}
