import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../common/widget/icon_theme.dart';
import '../../models/frais_en_forfait.dart';
import '../../services/frais_en_forfait.dart';

class AdminFraisForfait extends StatefulWidget {
  const AdminFraisForfait({Key? key}) : super(key: key);

  @override
  State<AdminFraisForfait> createState() => _AdminFraisForfaitState();
}

class _AdminFraisForfaitState extends State<AdminFraisForfait> {
  late Future<List<FraisEnForfait>?> listFraisEnForfait;
  var separator = false;

  @override
  initState() {
    super.initState();
    listFraisEnForfait = FraisEnForfaitService().fetchFraisEnForfait();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            leading: IconButton(
              icon: const Icon(Icons.logout_rounded),
              onPressed: () {
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/login', (Route<dynamic> route) => false);
              },
            ),
            title: const Center(child: Text('Gestion frais en forfait')),
            actions: const <Widget>[
              IconThemeApp(),
            ]),
        body: Stack(children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/FondNoir.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          FutureBuilder<List<FraisEnForfait>?>(
            future: listFraisEnForfait,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.separated(
                    padding: const EdgeInsets.only(bottom: 100),
                    itemBuilder: (context, index) {
                      var frais =
                          (snapshot.data as List<FraisEnForfait>)[index];
                      separator = false;
                      return Card(
                        margin: const EdgeInsets.symmetric(horizontal: 10),
                        child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Expanded(
                                        child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          frais.libelle,
                                          overflow: TextOverflow.ellipsis,
                                          softWrap: false,
                                          style: const TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        const SizedBox(height: 3),
                                        Text(
                                          frais.prixUnitaire.toString(),
                                          overflow: TextOverflow.ellipsis,
                                          softWrap: false,
                                        ),
                                      ],
                                    )),
                                    Column(
                                      children: <Widget>[
                                        InkWell(
                                          onTap: () {
                                            Navigator.of(context).pushNamed(
                                                '/updatefraisforfait',
                                                arguments: frais.id);
                                          },
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 12, vertical: 6),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: const Color.fromARGB(
                                                        255, 0, 121, 4)),
                                                borderRadius:
                                                    BorderRadius.circular(20)),
                                            child: const Text(
                                              'Editer',
                                              style: TextStyle(
                                                  fontSize: 10,
                                                  color: Color.fromARGB(
                                                      255, 0, 121, 4)),
                                            ),
                                          ),
                                        ),
                                        const SizedBox(height: 8),
                                        InkWell(
                                          onTap: () {
                                            FraisEnForfaitService()
                                                .deleteFraisEnForfait(frais.id)
                                                .then((value) {
                                              if (value.statusMessage == 'OK') {
                                                Fluttertoast.showToast(
                                                    msg: 'Utilisateur supprimé',
                                                    toastLength:
                                                        Toast.LENGTH_SHORT,
                                                    gravity:
                                                        ToastGravity.BOTTOM,
                                                    timeInSecForIosWeb: 10,
                                                    backgroundColor: Colors.red,
                                                    textColor: Colors.white,
                                                    fontSize: 16.0);
                                              } else {
                                                Fluttertoast.showToast(
                                                    msg: value.data['message'],
                                                    toastLength:
                                                        Toast.LENGTH_SHORT,
                                                    gravity:
                                                        ToastGravity.BOTTOM,
                                                    timeInSecForIosWeb: 10,
                                                    backgroundColor: Colors.red,
                                                    textColor: Colors.white,
                                                    fontSize: 16.0);
                                              }
                                            });
                                            Navigator.of(context).pushNamed(
                                                '/adminfraisforfait');
                                          },
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 10, vertical: 6),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: const Color.fromARGB(
                                                        255, 198, 13, 0)),
                                                borderRadius:
                                                    BorderRadius.circular(20)),
                                            child: const Text(
                                              'Supprimer',
                                              style: TextStyle(
                                                color: Color.fromARGB(
                                                    255, 198, 13, 0),
                                                fontSize: 12,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            )),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Divider(
                        color: Colors.transparent,
                        height: separator == true ? 0 : 10,
                      );
                    },
                    itemCount: (snapshot.data as List<FraisEnForfait>).length);
              } else if (snapshot.hasError) {
                return Center(child: Text('${snapshot.error}'));
              }
              return const Center(
                child: CircularProgressIndicator(
                    backgroundColor: Colors.cyanAccent),
              );
            },
          ),
        ]),
        floatingActionButton: Container(
          margin: const EdgeInsets.only(bottom: 10),
          child: FloatingActionButton.extended(
              onPressed: () {
                Navigator.of(context).pushNamed('/addfraisforfait');
              },
              icon: const Icon(
                Icons.add,
              ),
              label: const Text("Frais En Forfait")),
        ));
  }
}
