import 'package:flutter/material.dart';
import 'package:flutter_application_gsb/common/widget/icon_theme.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:select_form_field/select_form_field.dart';

import '../../services/utilisateur.dart';

class AddUser extends StatefulWidget {
  const AddUser({Key? key}) : super(key: key);

  @override
  State<AddUser> createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final RegExp emailRegex = RegExp(
      r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
  final RegExp passwordRegex =
      RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$');
  var data = {
    "nom": "",
    "prenom": "",
    "email": "",
    "identifiant": "",
    "motDePasse": "",
    "dateNaissance": "",
    "dateEmbauche": "",
    "adresse": "",
    "codePostal": "",
    "ville": "",
    "roleId": ""
  };

  final List<Map<String, dynamic>> _items = [
    {'value': 1, 'label': 'Visiteur'},
    {'value': 2, 'label': 'Comptable'},
    {'value': 3, 'label': 'Admin'},
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(actions: const <Widget>[IconThemeApp()]),
        body: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
            child: Column(children: <Widget>[
              const Text(
                'Ajouter un nouvel utilisateur',
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
              ),
              Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(height: 20),
                      TextFormField(
                        onChanged: (value) =>
                            setState(() => data["nom"] = value),
                        validator: (value) =>
                            value!.isEmpty ? 'Ce champ est requis' : null,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Nom*',
                            hintText: "Entrez le nom de l'utilisateur"),
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        onChanged: (value) =>
                            setState(() => data["prenom"] = value),
                        validator: (value) =>
                            value!.isEmpty ? 'Ce champ est requis' : null,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Prénom*',
                            hintText: "Entrez le prénom de l'utilisateur"),
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        onChanged: (value) =>
                            setState(() => data["email"] = value),
                        validator: (value) =>
                            value!.isEmpty || !emailRegex.hasMatch(value)
                                ? 'Veuillez entrer un email valide'
                                : null,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Email*',
                            hintText: "Entrez l'email de l'utilisateur"),
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        onChanged: (value) =>
                            setState(() => data["identifiant"] = value),
                        validator: (value) =>
                            value!.isEmpty ? 'Ce champ est requis' : null,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Identifiant*',
                            hintText: "Entrez l'identifiant de l'utilisateur"),
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        onChanged: (value) =>
                            setState(() => data["motDePasse"] = value),
                        validator: (value) => value!.isEmpty ||
                                !passwordRegex.hasMatch(value)
                            ? 'Votre mot de passe doit contenir au moins 8 caractères dont une majuscule, une minuscule, un chiffre et un des caractères spéciaux suivants: ! @ # \$ & * ~ '
                            : null,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            errorMaxLines: 4,
                            labelText: 'Mot de passe*',
                            hintText:
                                "Entrez le mot de passe de l'utilisateur"),
                      ),
                      const SizedBox(height: 20),
                      DateTimePicker(
                        initialValue: '',
                        firstDate: DateTime(2000),
                        lastDate: DateTime(2100),
                        dateLabelText: "Date de naissance de l'utilisateur",
                        onChanged: (value) =>
                            setState(() => data["dateNaissance"] = value),
                      ),
                      const SizedBox(height: 20),
                      DateTimePicker(
                        initialValue: '',
                        firstDate: DateTime(2000),
                        lastDate: DateTime(2100),
                        dateLabelText: "Date d'embauche de l'utilisateur*",
                        validator: (value) =>
                            value!.isEmpty ? 'Ce champ est requis' : null,
                        onChanged: (value) =>
                            setState(() => data["dateEmbauche"] = value),
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        onChanged: (value) =>
                            setState(() => data["adresse"] = value),
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Adresse',
                            hintText: "Entrez l'adresse de l'utilisateur"),
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        onChanged: (value) =>
                            setState(() => data["codePostal"] = value),
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Code postal',
                            hintText: "Entrez le code postal de l'utilisateur"),
                        maxLength: 5,
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        onChanged: (value) =>
                            setState(() => data["ville"] = value),
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Ville',
                            hintText: "Entrez la ville de l'utilisateur"),
                      ),
                      const SizedBox(height: 20),
                      SelectFormField(
                        labelText: "Role de l'utilisateur",
                        type: SelectFormFieldType.dropdown,
                        items: _items,
                        validator: (value) =>
                            value!.isEmpty ? 'Veuillez choisir un rôle' : null,
                        onChanged: (value) =>
                            setState(() => data["roleId"] = value),
                      ),
                      const SizedBox(height: 20),
                      Container(
                        height: 50,
                        width: 250,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20)),
                        child: ElevatedButton(
                          onPressed: () async {
                            if (_formKey.currentState!.validate()) {
                              await UtilisateurService()
                                  .addUser(data)
                                  .then((value) {
                                if (value.statusMessage == 'OK') {
                                  Fluttertoast.showToast(
                                      msg: 'Utilisateur créé avec succès',
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 10,
                                      backgroundColor: Colors.green,
                                      textColor: Colors.white,
                                      fontSize: 16.0);
                                  Navigator.of(context)
                                      .pushNamed('/dashboardadmin');
                                } else {
                                  Fluttertoast.showToast(
                                      msg: value.data['message'],
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 10,
                                      backgroundColor: Colors.red,
                                      textColor: Colors.white,
                                      fontSize: 16.0);
                                }
                              });
                            }
                          },
                          child: const Text(
                            'Ajouter',
                            style: TextStyle(fontSize: 25),
                          ),
                        ),
                      ),
                    ],
                  ))
            ])));
  }
}
