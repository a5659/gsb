import 'package:flutter/material.dart';
import 'package:flutter_application_gsb/common/widget/icon_theme.dart';
import 'package:flutter_application_gsb/models/utilisateur.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../services/utilisateur.dart';

class DashBoardAdmin extends StatefulWidget {
  const DashBoardAdmin({
    Key? key,
  }) : super(key: key);

  @override
  _DashBoardAdminState createState() => _DashBoardAdminState();
}

class _DashBoardAdminState extends State<DashBoardAdmin> {
  late Future<List<Utilisateur>?> listUsers;
  var separator = false;

  @override
  initState() {
    super.initState();
    listUsers = UtilisateurService().fetchUsers();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
              bottom: const TabBar(
                isScrollable: true,
                tabs: [
                  Tab(text: 'Visiteurs'),
                  Tab(text: 'Comptables'),
                  Tab(text: 'Administrateurs'),
                ],
              ),
              automaticallyImplyLeading: false,
              leading: IconButton(
                icon: const Icon(Icons.logout_rounded),
                onPressed: () {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/login', (Route<dynamic> route) => false);
                },
              ),
              title: const Center(child: Text('Admin Dashboard')),
              actions: const <Widget>[
                IconThemeApp(),
              ]),
          body: Stack(children: [
            Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/FondNoir.jpg"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            TabBarView(
              children: [
//_________________Liste des utilisateurs________________
                FutureBuilder<List<Utilisateur>?>(
                  future: listUsers,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return ListView.separated(
                          padding: const EdgeInsets.only(bottom: 100),
                          itemBuilder: (context, index) {
                            var user =
                                (snapshot.data as List<Utilisateur>)[index];
                            if (user.roleId == 1) {
                              separator = false;
                              String nomComplet = user.nom.toUpperCase() +
                                  " " +
                                  user.prenom[0].toUpperCase() +
                                  user.prenom.substring(1);
                              return Card(
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                child: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Expanded(
                                                child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  nomComplet,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  softWrap: false,
                                                  style: const TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                const SizedBox(height: 3),
                                                Text(
                                                  user.email ?? '',
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  softWrap: false,
                                                ),
                                              ],
                                            )),
                                            Column(
                                              children: <Widget>[
                                                InkWell(
                                                  onTap: () {
                                                    Navigator.of(context)
                                                        .pushNamed(
                                                            '/updateUser',
                                                            arguments: user.id);
                                                  },
                                                  child: Container(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 12,
                                                        vertical: 6),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: const Color
                                                                    .fromARGB(
                                                                255,
                                                                0,
                                                                121,
                                                                4)),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(20)),
                                                    child: const Text(
                                                      'Editer',
                                                      style: TextStyle(
                                                          fontSize: 10,
                                                          color: Color.fromARGB(
                                                              255, 0, 121, 4)),
                                                    ),
                                                  ),
                                                ),
                                                const SizedBox(height: 8),
                                                InkWell(
                                                  onTap: () {
                                                    UtilisateurService()
                                                        .deleteUser(user.id)
                                                        .then((value) {
                                                      if (value.statusMessage ==
                                                          'OK') {
                                                        Fluttertoast.showToast(
                                                            msg:
                                                                'Utilisateur supprimé',
                                                            toastLength: Toast
                                                                .LENGTH_SHORT,
                                                            gravity:
                                                                ToastGravity
                                                                    .BOTTOM,
                                                            timeInSecForIosWeb:
                                                                10,
                                                            backgroundColor:
                                                                Colors.red,
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 16.0);
                                                      } else {
                                                        Fluttertoast.showToast(
                                                            msg: value.data[
                                                                'message'],
                                                            toastLength: Toast
                                                                .LENGTH_SHORT,
                                                            gravity:
                                                                ToastGravity
                                                                    .BOTTOM,
                                                            timeInSecForIosWeb:
                                                                10,
                                                            backgroundColor:
                                                                Colors.red,
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 16.0);
                                                      }
                                                    });
                                                    Navigator.of(context)
                                                        .pushNamed('/admin');
                                                  },
                                                  child: Container(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 10,
                                                        vertical: 6),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: const Color
                                                                    .fromARGB(
                                                                255,
                                                                198,
                                                                13,
                                                                0)),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(20)),
                                                    child: const Text(
                                                      'Supprimer',
                                                      style: TextStyle(
                                                        color: Color.fromARGB(
                                                            255, 198, 13, 0),
                                                        fontSize: 12,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    )),
                              );
                            } else {
                              separator = true;
                              return const SizedBox(height: 0);
                            }
                          },
                          separatorBuilder: (context, index) {
                            return Divider(
                              color: Colors.transparent,
                              height: separator == true ? 0 : 10,
                            );
                          },
                          itemCount:
                              (snapshot.data as List<Utilisateur>).length);
                    } else if (snapshot.hasError) {
                      return Center(child: Text('${snapshot.error}'));
                    }
                    return const Center(
                      child: CircularProgressIndicator(
                          backgroundColor: Colors.cyanAccent),
                    );
                  },
                ),
//_________________Liste des comptables________________
                FutureBuilder<List<Utilisateur>?>(
                  future: listUsers,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return ListView.separated(
                          itemBuilder: (context, index) {
                            var user =
                                (snapshot.data as List<Utilisateur>)[index];
                            if (user.roleId == 2) {
                              separator = false;
                              String nomComplet = user.nom.toUpperCase() +
                                  " " +
                                  user.prenom[0].toUpperCase() +
                                  user.prenom.substring(1);
                              return Card(
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                child: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Expanded(
                                                child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  nomComplet,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  softWrap: false,
                                                  style: const TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                const SizedBox(height: 3),
                                                Text(
                                                  user.email ?? '',
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  softWrap: false,
                                                ),
                                              ],
                                            )),
                                            Column(
                                              children: <Widget>[
                                                InkWell(
                                                  onTap: () {
                                                    Navigator.of(context)
                                                        .pushNamed(
                                                            '/updateUser',
                                                            arguments: user.id);
                                                  },
                                                  child: Container(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 12,
                                                        vertical: 6),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: const Color
                                                                    .fromARGB(
                                                                255,
                                                                0,
                                                                121,
                                                                4)),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(20)),
                                                    child: const Text(
                                                      'Editer',
                                                      style: TextStyle(
                                                          fontSize: 10,
                                                          color: Color.fromARGB(
                                                              255, 0, 121, 4)),
                                                    ),
                                                  ),
                                                ),
                                                const SizedBox(height: 8),
                                                InkWell(
                                                  onTap: () {
                                                    UtilisateurService()
                                                        .deleteUser(user.id)
                                                        .then((value) {
                                                      if (value.statusMessage ==
                                                          'OK') {
                                                        Fluttertoast.showToast(
                                                            msg:
                                                                'Utilisateur supprimé',
                                                            toastLength: Toast
                                                                .LENGTH_SHORT,
                                                            gravity:
                                                                ToastGravity
                                                                    .BOTTOM,
                                                            timeInSecForIosWeb:
                                                                10,
                                                            backgroundColor:
                                                                Colors.red,
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 16.0);
                                                      } else {
                                                        Fluttertoast.showToast(
                                                            msg: value.data[
                                                                'message'],
                                                            toastLength: Toast
                                                                .LENGTH_SHORT,
                                                            gravity:
                                                                ToastGravity
                                                                    .BOTTOM,
                                                            timeInSecForIosWeb:
                                                                10,
                                                            backgroundColor:
                                                                Colors.red,
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 16.0);
                                                      }
                                                    });
                                                    Navigator.of(context)
                                                        .pushNamed('/admin');
                                                  },
                                                  child: Container(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 10,
                                                        vertical: 6),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: const Color
                                                                    .fromARGB(
                                                                255,
                                                                198,
                                                                13,
                                                                0)),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(20)),
                                                    child: const Text(
                                                      'Supprimer',
                                                      style: TextStyle(
                                                        color: Color.fromARGB(
                                                            255, 198, 13, 0),
                                                        fontSize: 12,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    )),
                              );
                            } else {
                              separator = true;
                              return const SizedBox(height: 0);
                            }
                          },
                          separatorBuilder: (context, index) {
                            return Divider(
                              color: Colors.transparent,
                              height: separator == true ? 0 : 10,
                            );
                          },
                          itemCount:
                              (snapshot.data as List<Utilisateur>).length);
                    } else if (snapshot.hasError) {
                      return Center(child: Text('${snapshot.error}'));
                    }
                    return const Center(
                      child: CircularProgressIndicator(
                          backgroundColor: Colors.cyanAccent),
                    );
                  },
                ),
//_________________Liste des administrateurs________________
                FutureBuilder<List<Utilisateur>?>(
                  future: listUsers,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return ListView.separated(
                          itemBuilder: (context, index) {
                            var user =
                                (snapshot.data as List<Utilisateur>)[index];
                            if (user.roleId == 3) {
                              separator = false;
                              String nomComplet = user.nom.toUpperCase() +
                                  " " +
                                  user.prenom[0].toUpperCase() +
                                  user.prenom.substring(1).toLowerCase();
                              return Card(
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                child: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Expanded(
                                                child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  nomComplet,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  softWrap: false,
                                                  style: const TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                const SizedBox(height: 3),
                                                Text(
                                                  user.email ?? '',
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  softWrap: false,
                                                ),
                                              ],
                                            )),
                                            Column(
                                              children: <Widget>[
                                                InkWell(
                                                  onTap: () {
                                                    Navigator.of(context)
                                                        .pushNamed(
                                                            '/updateUser',
                                                            arguments: user.id);
                                                  },
                                                  child: Container(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 12,
                                                        vertical: 6),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: const Color
                                                                    .fromARGB(
                                                                255,
                                                                0,
                                                                121,
                                                                4)),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(20)),
                                                    child: const Text(
                                                      'Editer',
                                                      style: TextStyle(
                                                          fontSize: 10,
                                                          color: Color.fromARGB(
                                                              255, 0, 121, 4)),
                                                    ),
                                                  ),
                                                ),
                                                const SizedBox(height: 8),
                                                InkWell(
                                                  onTap: () {
                                                    UtilisateurService()
                                                        .deleteUser(user.id)
                                                        .then((value) {
                                                      if (value.statusMessage ==
                                                          'OK') {
                                                        Fluttertoast.showToast(
                                                            msg:
                                                                'Utilisateur supprimé',
                                                            toastLength: Toast
                                                                .LENGTH_SHORT,
                                                            gravity:
                                                                ToastGravity
                                                                    .BOTTOM,
                                                            timeInSecForIosWeb:
                                                                10,
                                                            backgroundColor:
                                                                Colors.red,
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 16.0);
                                                      } else {
                                                        Fluttertoast.showToast(
                                                            msg: value.data[
                                                                'message'],
                                                            toastLength: Toast
                                                                .LENGTH_SHORT,
                                                            gravity:
                                                                ToastGravity
                                                                    .BOTTOM,
                                                            timeInSecForIosWeb:
                                                                10,
                                                            backgroundColor:
                                                                Colors.red,
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 16.0);
                                                      }
                                                    });
                                                    Navigator.of(context)
                                                        .pushNamed('/admin');
                                                  },
                                                  child: Container(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 10,
                                                        vertical: 6),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: const Color
                                                                    .fromARGB(
                                                                255,
                                                                198,
                                                                13,
                                                                0)),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(20)),
                                                    child: const Text(
                                                      'Supprimer',
                                                      style: TextStyle(
                                                        color: Color.fromARGB(
                                                            255, 198, 13, 0),
                                                        fontSize: 12,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    )),
                              );
                            } else {
                              separator = true;
                              return const SizedBox(height: 0);
                            }
                          },
                          separatorBuilder: (context, index) {
                            return Divider(
                              color: Colors.transparent,
                              height: separator == true ? 0 : 10,
                            );
                          },
                          itemCount:
                              (snapshot.data as List<Utilisateur>).length);
                    } else if (snapshot.hasError) {
                      return Center(child: Text('${snapshot.error}'));
                    }
                    return const Center(
                      child: CircularProgressIndicator(
                          backgroundColor: Colors.cyanAccent),
                    );
                  },
                ),
              ],
            )
          ]),
          floatingActionButton: Container(
            margin: const EdgeInsets.only(bottom: 10),
            child: FloatingActionButton.extended(
                onPressed: () {
                  Navigator.of(context).pushNamed('/addUser');
                },
                icon: const Icon(
                  Icons.add,
                ),
                label: const Text("User")),
          ),
        ));
  }
}
