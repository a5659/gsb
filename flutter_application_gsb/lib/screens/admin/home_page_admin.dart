import 'package:flutter/material.dart';

import '../../common/widget/icon_theme.dart';

class HomePageAdmin extends StatefulWidget {
  const HomePageAdmin({Key? key}) : super(key: key);

  @override
  State<HomePageAdmin> createState() => _HomePageAdminState();
}

class _HomePageAdminState extends State<HomePageAdmin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Center(
                child: Text(
              "Bienvenue !",
            )),
            leading: IconButton(
              icon: const Icon(Icons.logout_rounded),
              onPressed: () {
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/login', (Route<dynamic> route) => false);
              },
            ),
            actions: const <Widget>[IconThemeApp()]),
        body: Stack(children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/ADN.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  child: const Card(
                    child: SizedBox(
                        width: 150,
                        height: 150,
                        child: Center(
                            child: Text(
                          'DashBoard',
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ))),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/dashboardadmin');
                  },
                ),
                InkWell(
                  child: const Card(
                    child: SizedBox(
                        width: 150,
                        height: 150,
                        child: Center(
                            child: Text(
                          'Frais Forfait',
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ))),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/adminfraisforfait');
                  },
                ),
              ],
            )
          ])
        ]));
  }
}
