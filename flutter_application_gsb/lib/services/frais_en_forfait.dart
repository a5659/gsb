import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_storage/get_storage.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import '../models/frais_en_forfait.dart';

class FraisEnForfaitService {
  Dio dio = Dio();
  
  String routeAPI = dotenv.env['API_URL'] ?? "l'URL de l'API n'a pas été trouvée";

  final storage = GetStorage();

  Future<List<FraisEnForfait>?> fetchFraisEnForfait() async {
    try {
      Response<String> response = await dio.get(
        routeAPI + '/fraisEnForfait',
        options: Options(
          headers: {"x-access-token": storage.read('token'),}
        )
      );
      if (response.statusCode == 200) {
        String getFraisEnForfaitData = response.data.toString();
        final listFraisEnForfait =
            fraisEnForfaitFromJson(getFraisEnForfaitData);
        return listFraisEnForfait;
      } else {
        throw Exception('Failed to load frais');
      }
    } on DioError catch (e) {
      Fluttertoast.showToast(
          msg: e.response!.data['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    return null;
  }

  Future<FraisEnForfait?> fetchOneFraisEnForfait(id) async {
    try {
      Response<String> response =
          await dio.get(
            routeAPI + '/fraisEnForfait/$id',
            options: Options(
              headers: {"x-access-token": storage.read('token'),}
            )
          );
      if (response.statusCode == 200) {
        final fraisEnForfait =
            FraisEnForfait.fromJson((json.decode(response.data as String)));
        return fraisEnForfait;
      } else {
        throw Exception('Failed to load frais');
      }
    } on DioError catch (e) {
      Fluttertoast.showToast(
          msg: e.response!.data['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    return null;
  }

  addFraisEnForfait(fraisEnForfaitData) async {
    try {
      return await dio.post(
        routeAPI + '/fraisEnForfait',
          data: fraisEnForfaitData,
          options: Options(
            contentType: Headers.formUrlEncodedContentType,
            headers: {"x-access-token": storage.read('token')}
          )
        );
    } on DioError catch (e) {
      Fluttertoast.showToast(
          msg: e.response!.data['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  updateFraisEnForfait(fraisEnForfaitData, id) async {
    try {
      return await dio.put(
        routeAPI + '/fraisEnForfait/$id',
        data: fraisEnForfaitData,
        options: Options(
          contentType: Headers.formUrlEncodedContentType,
          headers: {"x-access-token": storage.read('token')}
        )
      );
    } on DioError catch (e) {
      Fluttertoast.showToast(
          msg: e.response!.data['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  deleteFraisEnForfait(id) async {
    try {
      return await dio.delete(
        routeAPI + '/fraisEnForfait/$id',
        options: Options(
          contentType: Headers.formUrlEncodedContentType,
          headers: {"x-access-token": storage.read('token')}
          )
        );
    } on DioError catch (e) {
      Fluttertoast.showToast(
          msg: e.response!.data['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
}
