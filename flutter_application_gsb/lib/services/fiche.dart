import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_storage/get_storage.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import '../models/fiche.dart';

class FicheService {
  Dio dio = Dio();

  String routeAPI = dotenv.env['API_URL'] ?? "l'URL de l'API n'a pas été trouvée";

  final storage = GetStorage();

  Future<Fiche?> fetchOneFiche(id) async {
    try {
      Response<String> response =
          await dio.get(
            routeAPI + '/fiches/$id',
            options: Options(
              headers: {"x-access-token": storage.read('token')}
            )
          );
      if (response.statusCode == 200) {
        final fiche =
            Fiche.fromJson((json.decode(response.data as String)));
        return fiche;
      } else {
        throw Exception('Failed to load frais');
      }
    } on DioError catch (e) {
      Fluttertoast.showToast(
          msg: e.response!.data['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    return null;
  }

  Future<List<Fiche>?> fetchFichesByVisitor(id) async {
    try {
      Response<String> response = await dio.get(
        "$routeAPI/fiches/visiteur/$id",
        options: Options(
          headers: {"x-access-token": storage.read('token')}
        )  
      );

      if (response.statusCode == 200) {
        String getFichesData = response.data.toString();
        final listFiche = ficheFromJson(getFichesData);
        return listFiche;
      } else {
        throw Exception("Echec durant le chargement des fiches pour le visiteur d'id $id");
      }
    } on DioError catch (e) {
      Fluttertoast.showToast(
        msg: e.response!.data['message'],
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
    }
    return null;
  }

  createFiche(payload) async {
    try {
      return await dio.post(
        "$routeAPI/fiches",
        data: payload,
        options: Options(
          headers: {"x-access-token": storage.read('token')}
        )      
      );
    } on DioError catch (e) {
      Fluttertoast.showToast(
        msg: e.response!.data['message'],
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
    }
    return null;
  }

  upadteFiche(id, payload) async {
    try {
      return await dio.put(
        "$routeAPI/fiches/$id",
        data: payload,
        options: Options(
          headers: {"x-access-token": storage.read('token')}
        )      
      );
    } on DioError catch (e) {
      Fluttertoast.showToast(
        msg: e.response!.data['message'],
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
    }
    return null;
  }
}
