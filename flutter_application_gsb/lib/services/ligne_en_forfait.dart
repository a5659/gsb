import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_storage/get_storage.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class LigneEnForfaitService {
  Dio dio = Dio();

  String routeAPI =
      dotenv.env['API_URL'] ?? "l'URL de l'API n'a pas été trouvée";

  final storage = GetStorage();

  createLigneEnForfait(payload) async {
    try {
      return await dio.post("$routeAPI/lignesFraisEnForfait",
          data: payload,
          options: Options(headers: {
            "x-access-token": storage.read('token'),
          }));
    } on DioError catch (e) {
      Fluttertoast.showToast(
          msg: e.response!.data['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    return null;
  }
}
