import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_storage/get_storage.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import '../models/utilisateur.dart';

class UtilisateurService {
  Dio dio = Dio();
  String routeAPI = dotenv.env['API_URL'] ?? "l'URL de l'API n'a pas été trouvée";

  final storage = GetStorage();

  Future<List<Utilisateur>?> fetchUsers() async {
    
    try {
      Response<String> response = await dio.get(
        routeAPI + '/utilisateurs',
        options: Options(
          headers: {"x-access-token": storage.read('token')}
        )
      );
      if (response.statusCode == 200) {
        String getUsersData = response.data.toString();
        final listUser = userFromJson(getUsersData);
        return listUser;
      } else {
        throw Exception('Failed to load users');
      }
    } on DioError catch (e) {
      Fluttertoast.showToast(
          msg: e.response!.data['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    return null;
  }

  Future<Utilisateur?> fetchOneUsers(id) async {
    try {
      Response<String> response = await dio.get(
        routeAPI + '/utilisateurs/$id',
        options: Options(
          headers: {"x-access-token": storage.read('token'),}
        )  
      );
      if (response.statusCode == 200) {
        final user = Utilisateur.fromJson(json.decode(response.data as String));
        return user;
      } else {
        throw Exception('Failed to load users');
      }
    } on DioError catch (e) {
      Fluttertoast.showToast(
          msg: e.response!.data['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    return null;
  }

  addUser(userData) async {
    try {
      return await dio.post(
        routeAPI + '/utilisateurs',
        data: userData,
        options: Options(
          contentType: Headers.formUrlEncodedContentType,
          headers: {"x-access-token": storage.read('token')},
        )
      );
    } on DioError catch (e) {
      Fluttertoast.showToast(
          msg: e.response!.data['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  updateUser(userData, id) async {
    try {
      return await dio.put(routeAPI + '/utilisateurs/$id',
          data: userData,
          options: Options(
            contentType: Headers.formUrlEncodedContentType,
            headers: {"x-access-token": storage.read('token')},
          )
        );
    } on DioError catch (e) {
      Fluttertoast.showToast(
          msg: e.response!.data['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  deleteUser(id) async {
    try {
      return await dio.delete(routeAPI + '/utilisateurs/$id',
          options: Options(
            contentType: Headers.formUrlEncodedContentType,
            headers: {"x-access-token": storage.read('token')},
          )
        );
    } on DioError catch (e) {
      Fluttertoast.showToast(
          msg: e.response!.data['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
}
