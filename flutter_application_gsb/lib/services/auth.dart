import 'package:dio/dio.dart';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class Auth {
  Dio dio = Dio();

  String routeAPI =
      dotenv.env['API_URL'] ?? "l'URL de l'API n'a pas été trouvée";

  login(identifiant, motDePasse) async {
    try {
      return await dio.post(routeAPI + '/login',
          data: {"identifiant": identifiant, "motDePasse": motDePasse},
          options: Options(
            contentType: Headers.formUrlEncodedContentType,
          ));
    } on DioError catch (e) {
      Fluttertoast.showToast(
          msg: e.response != null
              ? e.response!.data['message']
              : 'Veuillez verrifiez votre connexion à internet',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  getInfo(token) {
    Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
    return decodedToken;
  }
}
